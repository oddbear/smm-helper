import { FilterQuery, model, Schema } from 'mongoose';
import { Analytics } from '../types/Schemas.type';
import { ANALYTICS_EVENTS, FORMATS, GOODS, TONES } from '../utils/constants';
import { Languages } from '../utils/settings';
import { dictionary } from '../utils/dictionary';

export const analyticsSchema = {
  events: {
    required: false,
    default: [
      {
        eventName: 'join',
        count: 0,
      },
      {
        eventName: 'leave',
        count: 0,
      },
      {
        eventName: 'settingsClick',
        count: 0,
      },
      {
        eventName: 'projectsSettingsClick',
        count: 0,
      },
      {
        eventName: 'projectSettingsClick',
        count: 0,
        values: [
          {
            valueName: 'project-1',
            count: 0,
          },
          {
            valueName: 'project-2',
            count: 0,
          },
          {
            valueName: 'project-3',
            count: 0,
          },
        ],
      },
      {
        eventName: 'projectCreateClick',
        count: 0,
        values: [
          {
            valueName: 'project-1',
            count: 0,
          },
          {
            valueName: 'project-2',
            count: 0,
          },
          {
            valueName: 'project-3',
            count: 0,
          },
        ],
      },
      {
        eventName: 'projectCreate',
        count: 0,
        values: [
          {
            valueName: 'project-1',
            count: 0,
          },
          {
            valueName: 'project-2',
            count: 0,
          },
          {
            valueName: 'project-3',
            count: 0,
          },
        ],
      },
      {
        eventName: 'projectSetClick',
        count: 0,
        values: [
          {
            valueName: 'project-1',
            count: 0,
          },
          {
            valueName: 'project-2',
            count: 0,
          },
          {
            valueName: 'project-3',
            count: 0,
          },
        ],
      },
      {
        eventName: 'projectUnsetClick',
        count: 0,
        values: [
          {
            valueName: 'project-1',
            count: 0,
          },
          {
            valueName: 'project-2',
            count: 0,
          },
          {
            valueName: 'project-3',
            count: 0,
          },
        ],
      },
      {
        eventName: 'projectEditClick',
        count: 0,
        values: [
          {
            valueName: 'project-1',
            count: 0,
          },
          {
            valueName: 'project-2',
            count: 0,
          },
          {
            valueName: 'project-3',
            count: 0,
          },
        ],
      },
      {
        eventName: 'projectEdit',
        count: 0,
        values: [
          {
            valueName: 'project-1',
            count: 0,
          },
          {
            valueName: 'project-2',
            count: 0,
          },
          {
            valueName: 'project-3',
            count: 0,
          },
        ],
      },
      {
        eventName: 'projectDeleteClick',
        count: 0,
        values: [
          {
            valueName: 'project-1',
            count: 0,
          },
          {
            valueName: 'project-2',
            count: 0,
          },
          {
            valueName: 'project-3',
            count: 0,
          },
        ],
      },
      {
        eventName: 'projectDeleteConfirm',
        count: 0,
        values: [
          {
            valueName: 'project-1',
            count: 0,
          },
          {
            valueName: 'project-2',
            count: 0,
          },
          {
            valueName: 'project-3',
            count: 0,
          },
        ],
      },
      {
        eventName: 'postSettingsClick',
        count: 0,
      },
      {
        eventName: 'postChangeLanguage',
        count: 0,
        values: Object.keys(Languages).map((language) => ({
          valueName: language,
          count: 0,
        })),
      },
      {
        eventName: 'postUseTags',
        count: 0,
        values: [
          {
            valueName: 'use-tags',
            count: 0,
          },
          {
            valueName: 'not-use-tags',
            count: 0,
          },
        ],
      },
      {
        eventName: 'postUseVerbose',
        count: 0,
        values: [
          {
            valueName: 'use-verbose',
            count: 0,
          },
          {
            valueName: 'not-use-verbose',
            count: 0,
          },
        ],
      },
      {
        eventName: 'helpClick',
        count: 0,
      },
      {
        eventName: 'creditsClick',
        count: 0,
      },
      {
        eventName: 'creditsBuyClick',
        count: 0,
        values: GOODS.map((good) => ({
          valueName: good.label,
          count: 0,
        })),
      },
      {
        eventName: 'preCheckout',
        count: 0,
      },
      {
        eventName: 'purchase',
        count: 0,
        values: GOODS.map((good) => ({
          valueName: good.label,
          count: 0,
        })),
      },
      {
        eventName: 'generateClick',
        count: 0,
      },
      {
        eventName: 'generateCancel',
        count: 0,
      },
      {
        eventName: 'preGenerate',
        count: 0,
        values: [
          ...Object.keys(TONES).map((tone) => ({
            valueName: `tone-${tone}`,
            count: 0,
          })),
          ...Object.keys(FORMATS).map((format) => ({
            valueName: `format-${format}`,
            count: 0,
          })),
        ],
      },
      {
        eventName: 'generate',
        count: 0,
        values: [
          ...Object.keys(TONES).map((tone) => ({
            valueName: `tone-${tone}`,
            count: 0,
          })),
          ...Object.keys(FORMATS).map((format) => ({
            valueName: `format-${format}`,
            count: 0,
          })),
        ],
      },
      {
        eventName: 'repeat',
        count: 0,
      },
      {
        eventName: 'preGenerateImage',
        count: 0,
      },
      {
        eventName: 'generateImage',
        count: 0,
      },
      {
        eventName: 'repeatImage',
        count: 0,
      },
      {
        eventName: 'notEnoughCredits',
        count: 0,
      },
      {
        eventName: 'complain',
        count: 0,
      },
      {
        eventName: 'fulfillInfo',
        count: 0,
      },
      {
        eventName: 'fulfill',
        count: 0,
      },
      {
        eventName: 'preventCommand',
        count: 0,
      },
      {
        eventName: 'notFound',
        count: 0,
      },
    ],
    type: [
      {
        eventName: {
          required: true,
          type: String,
          enum: ANALYTICS_EVENTS,
        },
        count: {
          required: false,
          type: Number,
          default: 0,
        },
        values: {
          required: false,
          default: [],
          type: [
            {
              valueName: {
                required: true,
                type: String,
              },
              count: {
                required: false,
                type: Number,
                default: 0,
              },
            },
          ],
        },
      },
    ],
    createdMonth: {
      required: false,
      default: () => new Date().getMonth(),
      type: Number,
    },
  },
};

const schema = new Schema(analyticsSchema);

export const analyticsModel = model<Analytics>('Analytics', schema);

export const getAnalytics = async (): Promise<Analytics> => {
  const currentMonth = new Date().getMonth();
  let analytics = await analyticsModel.findOne({
    createdMonth: currentMonth,
  });

  if (analytics == null) {
    analytics = await analyticsModel.create({});
    console.log('analytics for current month not found, creating new');
  }

  return analytics;
};

export const updateAnalytics = async (data: FilterQuery<Analytics>) => {
  const analytics = await getAnalytics();
  return analyticsModel.updateOne({ createdMonth: analytics.createdMonth }, data);
};
