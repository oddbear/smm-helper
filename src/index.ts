import { CommandsService } from './services/commands.service';
import { ClusterService } from './services/cluster.service';
import { initializeMongo } from './utils/mongo';
import { CleanerService } from './services/cleaner.service';
import { AuthorizationService } from './services/authorization.service';
import { getBuildServices } from './utils/builder';
import { dictionary } from './utils/dictionary';
import { InlineHandlerService } from './services/inlineHandler.service';
import { PreventCommandService } from './services/prevent.service';
import express from 'express';
import { InputHandlerService } from './services/inputHandler.service';
import { NotFoundService } from './services/notFound.service';
import { ReplyHandlerService } from './services/replyHandler.service';
import { PreCheckoutService } from './services/preCheckout.service';
import { ProcessPaymentService } from './services/processPayment.service';

void (async function () {
  initializeMongo();

  const app = express();

  const { inputHandlerOptions, commandServiceStructure } = await getBuildServices();

  new ClusterService({
    services: [
      new AuthorizationService(),
      new PreCheckoutService(),
      new ProcessPaymentService(),
      new PreventCommandService(dictionary.generate.waitingForResponse),
      new ReplyHandlerService(
        `${dictionary.generate.backMessage}\n\n${dictionary.backBtnMessage}`,
        commandServiceStructure,
      ),
      new InlineHandlerService(),
      new CommandsService({
        structure: commandServiceStructure,
        defaultCommands: [
          {
            command: '/start',
            description: dictionary.defaultCommands.restart,
          },
          {
            command: '/help',
            description: dictionary.defaultCommands.help,
          },
        ],
      }),
      new InputHandlerService(inputHandlerOptions),
      new NotFoundService(),
      new CleanerService(),
    ],
  });

  app.get('/liveness', (req, res) => {
    res.status(200).send('ok');
  });
})();
