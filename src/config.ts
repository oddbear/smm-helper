import convict from 'convict';
import dotenv from 'dotenv';

dotenv.config({});

export const config = convict({
  env: {
    env: 'NODE_ENV',
    format: ['production', 'development', 'test'],
    default: 'development',
  },
  port: {
    env: 'PORT',
    format: Number,
    default: 4125,
  },
  telegram: {
    token: {
      doc: 'Telegram bot token',
      format: String,
      default: '',
      env: 'TELEGRAM_TOKEN',
    },
    paymentToken: {
      doc: 'Telegram bot payment token',
      format: String,
      default: '',
      env: 'PAYMENT_TOKEN',
    },
  },
  mongo: {
    username: {
      doc: 'Mongo username',
      format: String,
      default: '',
      env: 'MONGO_ROOT_USERNAME',
    },
    password: {
      doc: 'Mongo password',
      format: String,
      default: '',
      env: 'MONGO_ROOT_PASSWORD',
    },
  },
  openai: {
    token: {
      doc: 'OpenAI token',
      format: String,
      default: '',
      env: 'OPENAI_TOKEN',
    },
  },
  dreamstudio: {
    apiKey: {
      doc: 'DreamStudio API key',
      format: String,
      default: '',
      env: 'DREAMSTUDIO_API_KEY',
    },
  },
}).getProperties();
