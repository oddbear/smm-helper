import { ClusterServiceTool } from '../types/tools/ClusterService.tool';
import TelegramBot from 'node-telegram-bot-api';
import { addAnalyticsEvent } from '../utils/analytics';
import { GOODS } from '../utils/constants';
import { handleError } from '../utils/telegram';
import { dictionary } from '../utils/dictionary';
import { getUser, updateUser } from '../models/user.model';
import { EventNames } from '../types/ClusterContext.type';

export class ProcessPaymentService extends ClusterServiceTool {
  async onSuccessfulPayment(message: TelegramBot.Message) {
    const {
      chat: { id: chatId },
    } = message;
    const { context, telegramBot } = this.getOptions();

    const payload = message.successful_payment?.invoice_payload;
    if (payload == null) {
      await handleError(
        telegramBot,
        {
          chatId,
        },
        new Error(dictionary.credits.paymentError),
      );
      return true;
    }

    const good = GOODS.find(({ title }) => title === payload);
    if (good == null) {
      await handleError(
        telegramBot,
        {
          chatId,
        },
        new Error(dictionary.credits.paymentError),
      );
      return true;
    }

    const user = await getUser(chatId);
    if (user.paymentMessageId) {
      await updateUser(chatId, {
        $inc: {
          credits: good.creditsCount,
        },
        $pull: {
          removeMessagesIds: user.paymentMessageId,
        },
        paymentMessageId: null,
      });
    }

    void addAnalyticsEvent('purchase', good.label);

    const removeListener = context.addEventListener(EventNames.loopEnd, async () => {
      removeListener();

      const structureMessageId = user.structureMessageId;
      if (structureMessageId != null) {
        try {
          await telegramBot.deleteMessage(chatId, structureMessageId.toString());
        } catch (err) {}
      }

      await updateUser(chatId, {
        isWaitingForResponse: false,
        structureMessageId: null,
      });

      await context.dispatchCallbackQuery(context, {
        ...message,
        message,
        data: dictionary.credits.commandName,
      } as unknown as TelegramBot.CallbackQuery);
    });

    return true;
  }
}
