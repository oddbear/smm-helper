import { getUser, updateUser, updateUsers } from '../models/user.model';
import { ClusterServiceTool } from '../types/tools/ClusterService.tool';
import { SendCommandService } from './commands/send.command';
import TelegramBot from 'node-telegram-bot-api';
import { addAnalyticsEvent } from '../utils/analytics';
import { EventNames } from '../types/ClusterContext.type';

export class PreventCommandService extends ClusterServiceTool {
  private readonly sendCommandService: SendCommandService = new SendCommandService();

  constructor(private readonly message: string) {
    super();

    void updateUsers(
      {},
      {
        isWaitingForResponse: false,
      },
    );
  }

  setOptions(options): this {
    super.setOptions(options);

    this.sendCommandService.setOptions(options);

    return this;
  }

  public async onMessage(message) {
    const options = this.getOptions();
    const { context } = options;

    const user = await getUser(context.chatId);
    if (user.isWaitingForResponse) {
      const { sendCommandService } = this;
      sendCommandService.setOptions({
        ...options,
        message: this.message,
      });
      await sendCommandService.action(message);

      void addAnalyticsEvent('preventCommand');

      context.stopPropagation = true;
    } else {
      await updateUser(options.context.chatId, {
        isWaitingForResponse: true,
      });

      const removeListener = context.addEventListener(EventNames.loopEnd, async (curContext) => {
        if (curContext.contextId != context.contextId) {
          return;
        }

        removeListener();

        await updateUser(context.chatId, {
          isWaitingForResponse: false,
        });
      });
    }

    return user.isWaitingForResponse;
  }

  async onCallbackQuery(query: TelegramBot.CallbackQuery) {
    if (query.message != null) {
      return this.onMessage(query.message);
    }

    return false;
  }
}
