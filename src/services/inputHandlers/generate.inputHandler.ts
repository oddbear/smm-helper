import { InputHandlerServiceTool } from '../../types/tools/InputHandlerService.tool';
import TelegramBot from 'node-telegram-bot-api';
import { BackCommandService } from '../commands/back.command';
import { getUser, updateUser } from '../../models/user.model';
import { sendCompletionWithSideEffects } from '../../utils/ai';
import { DELAY_BEFORE_MENU_UPDATED_AFTER_GENERATION } from '../../utils/constants';
import { dictionary } from '../../utils/dictionary';
import { CommandsStructure } from '../../types/CommadsStructure.type';
import { User } from '../../types/Schemas.type';

export class GenerateInputHandlerService extends InputHandlerServiceTool {
  private disableRepeatButton: boolean = false;
  private customSettings: Partial<User['settings']> = {};
  private readonly backCommandService: BackCommandService = new BackCommandService({
    to: dictionary.startBot,
  });

  constructor(private readonly backCommandMessage: string) {
    super();
  }

  setOptions(
    opts: Parameters<InputHandlerServiceTool['setOptions']>[0] & {
      disableRepeatButton?: boolean;
      commandsStructure?: CommandsStructure;
      customSettings?: Partial<User['settings']>;
    },
  ): this {
    super.setOptions(opts);

    this.disableRepeatButton = opts.disableRepeatButton ?? this.disableRepeatButton;
    this.customSettings = opts.customSettings ?? this.customSettings;

    const options = this.getOptions();
    const { commandsStructure } = this.validateProperties({
      commandsStructure: opts.commandsStructure ?? options.context.commandsStructure,
    });

    this.backCommandService.setOptions({
      ...options,
      commandsStructure,
    });

    return this;
  }

  public async activate(msg: TelegramBot.Message): Promise<boolean> {
    const {
      text,
      message_id,
      chat: { id: chatId },
    } = msg;
    if (text == null) {
      return false;
    }

    const { context } = this.getOptions();
    const { telegramBot } = this.validateProperties({
      telegramBot: this.telegramBot,
    });

    const user = await getUser(chatId);
    user.settings = {
      ...user.settings,
      ...this.customSettings,
    };

    context.dontRemoveMessage = true;

    await sendCompletionWithSideEffects(
      {
        telegramBot,
        context,
        user,
        replyId: message_id,
        text,
        disableRepeatButton: this.disableRepeatButton,
      },
      async (err) => {
        if (err == null) {
          this.backCommandService.setOptions({
            message: this.backCommandMessage,
          });
          await delayedActionOnBackCommand(this.backCommandService, msg);
          return;
        }

        this.backCommandService.setOptions({
          message: dictionary.backBtnMessage,
        });
        await this.backCommandService.action(msg);
      },
    );

    return true;
  }
}

export const delayedActionOnBackCommand = (backCommandService: BackCommandService, message: TelegramBot.Message) =>
  new Promise<void>((res) => {
    setTimeout(async () => {
      await backCommandService.action(message);
      res();
    }, DELAY_BEFORE_MENU_UPDATED_AFTER_GENERATION);
  });
