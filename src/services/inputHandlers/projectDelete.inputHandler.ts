import { InputHandlerServiceTool } from '../../types/tools/InputHandlerService.tool';
import TelegramBot from 'node-telegram-bot-api';
import { SendCommandService } from '../commands/send.command';
import { BackCommandService } from '../commands/back.command';
import { dictionary } from '../../utils/dictionary';
import { getUser, updateUser } from '../../models/user.model';
import { DELETE_KEY_WORD } from '../../utils/constants';
import { addAnalyticsEvent } from '../../utils/analytics';
import { Project } from '../../types/Schemas.type';

export class ProjectDeleteInputHandlerService extends InputHandlerServiceTool {
  private readonly sendCommandService: SendCommandService = new SendCommandService({
    message: dictionary.settings.projects.project.delete.invalid,
  });
  private readonly backCommandService: BackCommandService = new BackCommandService({
    to: dictionary.settings.projects.commandName,
  });

  setOptions(opts) {
    super.setOptions(opts);

    const options = this.getOptions();
    const { properties } = options;
    const { commandsStructure } = this.validateProperties({
      commandsStructure: options.context.commandsStructure,
    });

    if (properties?.project != null) {
      const project = properties.project as Project;

      this.backCommandService.setOptions({
        ...options,
        commandsStructure,
        message: `${dictionary.placeholders.choose(
          dictionary.settings.projects.commandName,
        )}\n\n${dictionary.settings.projects.project.delete.success(project.name)}`,
      });
    }
    this.sendCommandService.setOptions(options);

    return this;
  }

  public async activate(msg: TelegramBot.Message): Promise<boolean> {
    const {
      text: keyWord,
      chat: { id: chatId },
    } = msg;
    if (keyWord?.toLowerCase() !== DELETE_KEY_WORD.toLowerCase()) {
      this.sendCommandService.setOptions({
        message: dictionary.settings.projects.project.delete.invalid,
      });
      await this.sendCommandService.action(msg);

      return false;
    }

    const { properties } = this.getOptions();
    if (properties?.project == null) {
      return false;
    }

    const project = properties.project as Project;

    const user = await getUser(chatId);
    const { settings } = user;
    const { projects } = settings;
    const projectIndex = projects.findIndex((userProject) => userProject.name === project.name);

    void addAnalyticsEvent('projectDeleteConfirm', `project-${projectIndex + 1}`);

    if (projectIndex === -1) {
      throw new Error('Project not found');
    }

    if (settings.currentProjectName === projects[projectIndex].name) {
      settings.currentProjectName = null;
    }

    projects.splice(projectIndex, 1);
    await updateUser(chatId, {
      settings,
    });

    await this.backCommandService.action(msg);

    return true;
  }
}
