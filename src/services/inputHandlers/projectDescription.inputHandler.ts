import { InputHandlerServiceTool } from '../../types/tools/InputHandlerService.tool';
import TelegramBot from 'node-telegram-bot-api';
import { SendCommandService } from '../commands/send.command';
import { BackCommandService } from '../commands/back.command';
import { dictionary } from '../../utils/dictionary';
import { getUser, updateUser } from '../../models/user.model';
import { MAX_SYMBOLS_IN_PROJECT_DESCRIPTION } from '../../utils/constants';
import { addAnalyticsEvent } from '../../utils/analytics';
import { Project } from '../../types/Schemas.type';

export class ProjectDescriptionInputHandlerService extends InputHandlerServiceTool {
  private readonly sendCommandService: SendCommandService = new SendCommandService({
    message: dictionary.settings.projects.project.edit.invalid,
  });
  private readonly backCommandService: BackCommandService = new BackCommandService({
    to: dictionary.settings.projects.commandName,
  });

  setOptions(opts) {
    super.setOptions(opts);

    const options = this.getOptions();
    const { properties } = options;
    const { commandsStructure } = this.validateProperties({
      commandsStructure: options.context.commandsStructure,
    });

    if (properties?.project != null) {
      const project = properties.project as Project;
      this.backCommandService.setOptions({
        ...options,
        commandsStructure,
        message: `${dictionary.placeholders.choose(
          dictionary.settings.projects.commandName,
        )}\n\n${dictionary.settings.projects.project.edit.success(project.name)}`,
      });
    }
    this.sendCommandService.setOptions(options);

    return this;
  }

  public async activate(msg: TelegramBot.Message): Promise<boolean> {
    const {
      text: projectDescription,
      chat: { id: chatId },
    } = msg;
    if (projectDescription == null) {
      return false;
    }

    if (projectDescription.length > MAX_SYMBOLS_IN_PROJECT_DESCRIPTION) {
      this.sendCommandService.setOptions({
        message: dictionary.settings.projects.project.edit.invalid,
      });
      await this.sendCommandService.action(msg);

      return false;
    }

    const { properties } = this.getOptions();
    const { project } = this.validateProperties({
      project: properties?.project as Project,
    });

    const user = await getUser(chatId);
    const projectIndex = user.settings.projects.findIndex((userProject) => userProject.name === project.name);
    void addAnalyticsEvent('projectEdit', `project-${projectIndex + 1}`);

    await updateUser(user.chatId, {
      settings: {
        ...user.settings,
        projects: user.settings.projects.map((userProject) => {
          if (userProject.name === project.name) {
            return {
              ...userProject,
              description: projectDescription,
            };
          }

          return userProject;
        }),
      },
    });

    await this.backCommandService.action(msg);

    return true;
  }
}
