import { ClusterServiceTool } from '../types/tools/ClusterService.tool';
import TelegramBot from 'node-telegram-bot-api';
import {
  ADD_IMAGES_COMMAND,
  CHANGE_LANGUAGE_COMMAND,
  COMPLAIN_COMMAND,
  DELAY_BEFORE_MENU_UPDATED_AFTER_GENERATION,
  FORMATS,
  FULFILL_COMPLETION_COMMAND,
  HIDE_ERROR_COMMAND,
  MORE_COMMAND,
  PRICE_FOR_CREDITS,
  REPEAT_COMPLETION_COMMAND,
  REPEAT_IMAGE_COMMAND,
  TONES,
  USE_TAGS_COMMAND,
  USE_VERBOSE,
} from '../utils/constants';
import { getUser, updateUser } from '../models/user.model';
import { getAIPromptToImage, sendCompletionWithSideEffects } from '../utils/ai';
import { getNextLanguage } from '../utils/settings';
import { getUserPostSettingsInlineKeyboard } from './commands/postSettings.command';
import { EventNames } from '../types/ClusterContext.type';
import { getFormatsInlineKeyboard } from './commands/formatSettings.command';
import { getTonesInlineKeyboard } from './commands/toneSettings.command';
import { dictionary } from '../utils/dictionary';
import { addAnalyticsEvent } from '../utils/analytics';
import debounce from 'lodash/debounce';
import { generateImage } from '../utils/midjourney';
import { handleError } from '../utils/telegram';
import { parseUserSettingsFromText } from '../utils/data';

const tones = new Map(Object.entries(TONES).map((arr) => arr.reverse()) as [string, string][]);
const formats = new Map(Object.entries(FORMATS).map((arr) => arr.reverse()) as [string, string][]);

const changeLanguageDebounce = debounce((language: string) => {
  void addAnalyticsEvent('postChangeLanguage', language);
}, 3000);

export class InlineHandlerService extends ClusterServiceTool {
  async onCallbackQuery(query: TelegramBot.CallbackQuery) {
    const { data, message } = query;
    if (data == null || message?.chat == null) {
      return false;
    }

    if (data === HIDE_ERROR_COMMAND) {
      return true;
    }

    const { context } = this.getOptions();
    const { telegramBot } = this.validateProperties({
      telegramBot: this.telegramBot,
    });

    const user = await getUser(message.chat.id);

    if (data === ADD_IMAGES_COMMAND || data === REPEAT_IMAGE_COMMAND) {
      let message_id: number | undefined;
      let text: string | undefined;
      if (data === ADD_IMAGES_COMMAND) {
        if (message.text == null) {
          return false;
        }

        message_id = message.message_id;
        text = message.text;
      }
      if (data === REPEAT_IMAGE_COMMAND) {
        if (message.reply_to_message?.text == null) {
          return false;
        }

        message_id = message.reply_to_message.message_id;
        text = message.reply_to_message.text;
      }

      if (message_id == null || text == null) {
        return false;
      }

      context.dontRemoveMessage = true;

      const {
        chat: { id: chatId },
      } = message;

      if (user.credits < PRICE_FOR_CREDITS.image) {
        void addAnalyticsEvent('notEnoughCredits');
        const removeMessage = await telegramBot.sendMessage(
          chatId,
          dictionary.generate.noCredits.text,
          dictionary.generate.noCredits,
        );

        const removeListener = context.addEventListener(EventNames.loopEnd, async () => {
          removeListener();

          await updateUser(user.chatId, {
            $push: {
              removeMessagesIds: removeMessage.message_id,
            },
          });
        });

        return true;
      }

      void addAnalyticsEvent('preGenerateImage');

      try {
        if (user.structureMessageId != null) {
          await telegramBot.deleteMessage(user.chatId, user.structureMessageId.toString());
        }
      } catch (err) {}
      await updateUser(user.chatId, {
        structureMessageId: null,
      });

      const response = await telegramBot.sendMessage(user.chatId, dictionary.generate.getImages.generationWaiting, {
        reply_to_message_id: message_id,
      });

      context.dontRemoveMessage = true;

      try {
        const aiPromptToImage = await getAIPromptToImage(text).catch(async (err) => {
          await handleError(
            telegramBot,
            {
              chatId,
              text: response.text,
              replyId: response.message_id,
            },
            err,
          );
          return null;
        });
        if (aiPromptToImage == null) {
          return false;
        }

        console.log('AI Prompt to image:', aiPromptToImage);

        await generateImage(telegramBot, user, message_id, aiPromptToImage)
          .then(async () => {
            try {
              await telegramBot.deleteMessage(user.chatId, response.message_id.toString());
            } catch (err) {}

            void context.dispatchCallbackQuery(context, {
              ...query,
              data: dictionary.backBtn,
            });
          })
          .catch(async (err) => {
            await handleError(
              telegramBot,
              {
                chatId,
                text: response.text,
                replyId: response.message_id,
              },
              err,
            );

            await context.dispatchCallbackQuery(context, {
              ...query,
              data: dictionary.backBtn,
            });

            const removeListener = context.addEventListener(EventNames.loopEnd, async () => {
              removeListener();

              await updateUser(user.chatId, {
                $push: {
                  removeMessagesIds: response.message_id,
                },
              });
            });
          });

        void addAnalyticsEvent('generateImage');
      } catch (err) {
        await handleError(
          telegramBot,
          {
            chatId,
            text: response.text,
            replyId: response.message_id,
          },
          err as Error,
        );
      }

      return true;
    }

    if (data === REPEAT_COMPLETION_COMMAND) {
      if (message.reply_to_message?.text == null) {
        const responseId = await handleError(
          telegramBot,
          {
            chatId: user.chatId,
          },
          new Error('No message to reply'),
        );

        const removeListener = context.addEventListener(EventNames.loopEnd, async () => {
          removeListener();

          await updateUser(user.chatId, {
            $push: {
              removeMessagesIds: responseId,
            },
          });
        });

        return false;
      }

      if (message.text == null) {
        return false;
      }

      const {
        reply_to_message: { message_id, text },
        text: messageText,
      } = message;

      context.dontRemoveMessage = true;

      void addAnalyticsEvent('repeat');

      user.settings = parseUserSettingsFromText(user.settings, messageText);

      await sendCompletionWithSideEffects(
        {
          telegramBot,
          context,
          user,
          replyId: message_id,
          text,
        },
        async (err) => {
          setTimeout(
            () => {
              void context.dispatchCallbackQuery(context, {
                ...query,
                data: dictionary.backBtn,
              });
            },
            err == null ? DELAY_BEFORE_MENU_UPDATED_AFTER_GENERATION : 0,
          );
        },
      );

      return true;
    }

    if (data === COMPLAIN_COMMAND) {
      if (message.text == null || message.reply_markup?.inline_keyboard == null) {
        return false;
      }

      const {
        chat: { id: chatId },
        message_id,
        text,
        reply_markup,
      } = message;

      await telegramBot.editMessageText(text, {
        chat_id: chatId,
        message_id,
        reply_markup: {
          inline_keyboard: reply_markup.inline_keyboard.filter(
            (row) => row[0].text !== dictionary.generate.complain.commandName,
          ),
        },
      });

      await telegramBot.sendMessage(chatId, dictionary.generate.complain.success, {
        reply_to_message_id: message_id,
      });

      context.dontRemoveMessage = true;

      void addAnalyticsEvent('complain');

      const { structureMessageId } = user;
      await updateUser(chatId, {
        structureMessageId: null,
      });

      if (structureMessageId != null) {
        await this.telegramBot?.deleteMessage(chatId, structureMessageId.toString());

        await context.dispatchCallbackQuery(context, {
          ...query,
          data: dictionary.backBtn,
        });
      }

      return true;
    }

    if (data === FULFILL_COMPLETION_COMMAND) {
      if (message.reply_to_message?.text == null || message.text == null) {
        return false;
      }

      void addAnalyticsEvent('fulfillInfo');

      context.dontRemoveMessage = true;

      const response = await telegramBot.sendMessage(user.chatId, dictionary.generate.fulfillInfo);

      const removeListener = context.addEventListener(EventNames.loopEnd, async () => {
        removeListener();

        await updateUser(user.chatId, {
          $push: {
            removeMessagesIds: response.message_id,
          },
        });
      });

      return true;
    }

    if ([CHANGE_LANGUAGE_COMMAND, USE_TAGS_COMMAND, USE_VERBOSE].includes(data)) {
      const {
        chat: { id: chatId },
        text,
        message_id,
      } = message;

      if (text == null) {
        return false;
      }

      if (data === CHANGE_LANGUAGE_COMMAND) {
        user.settings.language = getNextLanguage(user);

        void changeLanguageDebounce(user.settings.language);
      }
      if (data === USE_VERBOSE) {
        user.settings.verbose = !user.settings.verbose;

        void addAnalyticsEvent('postUseVerbose', user.settings.verbose ? 'use-verbose' : 'not-use-verbose');
      }
      if (data === USE_TAGS_COMMAND) {
        user.settings.useTags = !user.settings.useTags;

        void addAnalyticsEvent('postUseTags', user.settings.useTags ? 'use-tags' : 'not-use-tags');
      }

      await updateUser(chatId, {
        settings: user.settings,
        $pull: {
          removeMessagesIds: message_id,
        },
      });
      context.dontRemoveMessage = true;

      const removeEventListener = context.addEventListener(EventNames.loopEnd, async () => {
        await updateUser(chatId, {
          $push: {
            removeMessagesIds: message_id,
          },
        });
        removeEventListener();
      });

      await telegramBot.editMessageText(text, {
        message_id,
        chat_id: chatId,
        reply_markup: {
          inline_keyboard: getUserPostSettingsInlineKeyboard(user),
        },
      });

      return true;
    }

    if (data.includes(MORE_COMMAND)) {
      if (message.text == null) {
        return false;
      }
      const {
        text,
        message_id,
        chat: { id: chatId },
      } = message;

      const moreType = data.split('-')[1];

      let inlineKeyboard;
      if (moreType === '🎵') {
        inlineKeyboard = await getTonesInlineKeyboard(user, Infinity);
      }
      if (moreType === '📝') {
        inlineKeyboard = await getFormatsInlineKeyboard(user, Infinity);
      }
      if (inlineKeyboard == null) {
        return false;
      }

      await telegramBot.editMessageText(text, {
        message_id,
        chat_id: chatId,
        reply_markup: {
          inline_keyboard: inlineKeyboard,
        },
      });

      const { removeMessagesIds } = user;
      await updateUser(chatId, {
        removeMessagesIds: [],
      });

      const removeListener = context.addEventListener(EventNames.loopEnd, () => {
        void updateUser(chatId, {
          removeMessagesIds,
        });

        removeListener();
      });

      context.dontRemoveMessage = true;

      // return true;
    }

    if (tones.has(data) || formats.has(data)) {
      if (message.text == null || message.reply_markup == null) {
        return false;
      }

      const {
        chat: { id: chatId },
        message_id,
        text,
        reply_markup: { inline_keyboard },
      } = message;

      const hasMoreButton = inline_keyboard
        .flat()
        .some((keyboard) => keyboard.text.includes(dictionary.placeholders.more('').commandName));

      const removeMessagesIds = user.removeMessagesIds;

      if (tones.has(data)) {
        const toneName = tones.get(data);
        if (toneName == null) {
          return false;
        }

        const isToneSet = user.settings.tone === toneName;
        user.settings.tone = isToneSet ? null : toneName;
        await updateUser(chatId, {
          settings: user.settings,
          removeMessagesIds: [],
        });

        await telegramBot.editMessageText(text, {
          message_id,
          chat_id: chatId,
          reply_markup: {
            inline_keyboard: await getTonesInlineKeyboard(user, hasMoreButton ? undefined : Infinity),
          },
        });
      }
      if (formats.has(data)) {
        const formatName = formats.get(data);
        if (formatName == null) {
          return false;
        }
        const isFormatSet = user.settings.format === formatName;

        user.settings.format = isFormatSet ? null : formatName;
        await updateUser(chatId, {
          settings: user.settings,
          removeMessagesIds: [],
        });

        await telegramBot.editMessageText(text, {
          message_id,
          chat_id: chatId,
          reply_markup: {
            inline_keyboard: await getFormatsInlineKeyboard(user, hasMoreButton ? undefined : Infinity),
          },
        });
      }

      context.dontRemoveMessage = true;

      const removeEventListener = context.addEventListener(EventNames.loopEnd, async () => {
        await updateUser(chatId, {
          removeMessagesIds,
        });
        removeEventListener();
      });

      // return true;
    }

    return false;
  }
}
