import { ClusterServiceTool } from '../types/tools/ClusterService.tool';
import TelegramBot from 'node-telegram-bot-api';
import { addAnalyticsEvent } from '../utils/analytics';

export class PreCheckoutService extends ClusterServiceTool {
  async onPreCheckoutQuery(query: TelegramBot.PreCheckoutQuery) {
    const { telegramBot } = this.getOptions();

    try {
      void addAnalyticsEvent('preCheckout');
      await telegramBot.answerPreCheckoutQuery(query.id, true);
    } catch (err) {}

    return true;
  }
}
