import { ClusterServiceTool } from '../types/tools/ClusterService.tool';
import TelegramBot from 'node-telegram-bot-api';
import { createUser, getUser } from '../models/user.model';
import { addAnalyticsEvent } from '../utils/analytics';

export class AuthorizationService extends ClusterServiceTool {
  private cachedUsers = new Set<number>();

  async onMessage({ message_id, chat: { id: chatId } }: TelegramBot.Message) {
    const cachedUsers = this.cachedUsers;
    if (cachedUsers.has(chatId)) {
      return false;
    } else {
      cachedUsers.add(chatId);
    }

    try {
      await getUser(chatId);
    } catch (err) {
      await createUser(chatId);

      void addAnalyticsEvent('join');
    }

    return false;
  }

  async onCallbackQuery(query: TelegramBot.CallbackQuery) {
    if (query.message != null) {
      return this.onMessage(query.message);
    }

    return false;
  }
}
