import { CommandServiceTool } from '../../types/tools/CommandService.tool';
import { getUser, updateUser } from '../../models/user.model';
import TelegramBot from 'node-telegram-bot-api';

export class ResendStructureCommand extends CommandServiceTool {
  async action(message: TelegramBot.Message) {
    const {
      chat: { id: chatId },
    } = message;
    const { telegramBot, context } = this.getOptions();

    const user = await getUser(message.chat.id);

    const structureMessageId = user.structureMessageId;
    if (structureMessageId != null) {
      await updateUser(chatId, {
        structureMessageId: null,
      });
      await telegramBot.deleteMessage(chatId, structureMessageId.toString());

      await context.dispatchCallbackQuery(context, {
        ...message,
        message,
        data: 'structure',
      } as unknown as TelegramBot.CallbackQuery);
    }

    context.stopPropagation = true;
  }
}
