import { CommandServiceTool } from '../../types/tools/CommandService.tool';
import { StructureCommandService } from './structure.command';
import { findCommandStructure } from '../../utils/data';
import { UnionMessageType } from '../../types/Message.type';

export class BackCommandService extends CommandServiceTool {
  private readonly structureCommandService: StructureCommandService = new StructureCommandService();
  private to: string | null = null;
  private message: UnionMessageType | null = null;

  constructor(options: { to: string; message?: UnionMessageType | null }) {
    super();

    this.to = options.to;
    this.message = options.message === null ? null : options.message ?? this.message;
  }

  setOptions(
    options: Parameters<CommandServiceTool['setOptions']>[number] & {
      to?: string | null;
      message?: UnionMessageType | null;
    },
  ) {
    super.setOptions(options);

    this.to = options.to === null ? null : options.to ?? this.to;
    this.message = options.message === null ? null : options.message ?? this.message;

    return this;
  }

  async action(message) {
    const { commandsStructure, context } = this.getOptions();

    if (this.to == null) {
      throw new Error('BackCommandService: no "to" option');
    }

    const { commandStructure } = this.validateProperties({
      commandStructure: await findCommandStructure({
        message,
        command: this.to,
        commandsStructure: commandsStructure!,
        context,
      }),
    });

    return this.getStructureCommandService().action(message, {
      ...commandStructure,
      saveMessage: false,
    });
  }

  private getStructureCommandService(): StructureCommandService {
    this.structureCommandService.setOptions({
      ...this.getOptions(),
      message: this.message,
    });

    return this.structureCommandService;
  }
}
