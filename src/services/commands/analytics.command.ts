import { CommandServiceTool } from '../../types/tools/CommandService.tool';
import { AnalyticsEvent } from '../../utils/constants';
import { addAnalyticsEvent } from '../../utils/analytics';

export class AnalyticsCommandService extends CommandServiceTool {
  constructor(private readonly eventName: AnalyticsEvent, private readonly valueName?: string) {
    super();
  }

  async action(msg) {
    await addAnalyticsEvent(this.eventName, this.valueName);
  }
}
