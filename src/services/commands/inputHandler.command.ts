import { CommandServiceTool } from '../../types/tools/CommandService.tool';
import { InputHandlerCommand } from '../../utils/constants';
import { EventNames } from '../../types/ClusterContext.type';
import { updateUser } from '../../models/user.model';

export class InputHandlerCommandService extends CommandServiceTool {
  constructor(
    private readonly command: InputHandlerCommand,
    private readonly options: { [key: string]: unknown } | null = null,
  ) {
    super();
  }

  public async action(message) {
    const {
      chat: { id: chatId },
    } = message;
    const { context } = this.getOptions();

    const removeListener = context.addEventListener(EventNames.loopEnd, async () => {
      await updateUser(chatId, {
        inputHandler: {
          command: this.command,
          options: this.options,
        },
      });
      removeListener();
    });
  }
}
