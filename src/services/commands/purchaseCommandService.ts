import { CommandServiceTool } from '../../types/tools/CommandService.tool';
import { handleError, sendInvoice } from '../../utils/telegram';
import { updateUser } from '../../models/user.model';
import { EventNames } from '../../types/ClusterContext.type';
import { dictionary } from '../../utils/dictionary';
import TelegramBot from 'node-telegram-bot-api';

export class PurchaseCommandService extends CommandServiceTool {
  private readonly title: string;
  private readonly label: string;
  private readonly amount: number;

  constructor(options: { title: string; label: string; amount: number }) {
    super();

    this.title = options.title;
    this.label = options.label;
    this.amount = options.amount;
  }

  async action(msg: TelegramBot.Message) {
    const { context, telegramBot } = this.getOptions();

    const result = await sendInvoice(
      msg.chat.id,
      this.title,
      dictionary.credits.description,
      this.title,
      'startParam',
      'USD',
      [
        {
          label: this.label,
          amount: this.amount,
        },
      ],
    ).catch(async (err: Error) => {
      const responseId = await handleError(
        telegramBot,
        {
          chatId: msg.chat.id,
        },
        err,
      );

      const removeListener = context.addEventListener(EventNames.loopEnd, async () => {
        removeListener();

        await updateUser(msg.chat.id, {
          $push: {
            removeMessagesIds: responseId,
          },
        });
      });

      return null;
    });

    if (result == null) {
      return;
    }

    const removeListener = context.addEventListener(EventNames.loopEnd, () => {
      removeListener();

      const messageId = result.data.result.message_id;
      void updateUser(msg.chat.id, {
        paymentMessageId: messageId,
        $push: {
          removeMessagesIds: messageId,
        },
      });
    });
  }
}
