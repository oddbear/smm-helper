import { CommandServiceTool } from '../../types/tools/CommandService.tool';
import { dictionary } from '../../utils/dictionary';
import { getUser } from '../../models/user.model';
import { FORMATS } from '../../utils/constants';
import { SendCommandService } from './send.command';
import { getSortedByRatingInlineKeyboard } from '../../utils/data';
import { User } from '../../types/Schemas.type';

export const getFormatsInlineKeyboard = async (user: User, maxLength?: number) => {
  return getSortedByRatingInlineKeyboard(user, FORMATS, 'format', maxLength);
};

export class FormatSettingsCommand extends CommandServiceTool {
  private readonly sendCommandService: SendCommandService = new SendCommandService();

  setOptions(options): this {
    super.setOptions(options);

    this.sendCommandService.setOptions(options);

    return this;
  }

  public async action(msg) {
    const { context } = this.getOptions();
    const { sendCommandService } = this;

    const user = await getUser(context.chatId);

    sendCommandService.setOptions({
      message: {
        text: dictionary.generate.format.message,
        reply_markup: {
          inline_keyboard: await getFormatsInlineKeyboard(user),
        },
      },
    });
    await sendCommandService.action(msg);
  }
}
