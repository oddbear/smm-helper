import { CommandServiceTool } from '../../types/tools/CommandService.tool';
import { dictionary } from '../../utils/dictionary';
import { getUser } from '../../models/user.model';
import { TONES } from '../../utils/constants';
import { SendCommandService } from './send.command';
import { getSortedByRatingInlineKeyboard } from '../../utils/data';

export const getTonesInlineKeyboard = (user, maxLength?: number) => {
  return getSortedByRatingInlineKeyboard(user, TONES, 'tone', maxLength);
};

export class ToneSettingsCommand extends CommandServiceTool {
  private readonly sendCommandService: SendCommandService = new SendCommandService();

  setOptions(options): this {
    super.setOptions(options);

    this.sendCommandService.setOptions(options);

    return this;
  }

  public async action(msg) {
    const { context } = this.getOptions();
    const { sendCommandService } = this;

    const user = await getUser(context.chatId);

    sendCommandService.setOptions({
      message: {
        text: dictionary.generate.tone.message,
        reply_markup: {
          inline_keyboard: await getTonesInlineKeyboard(user),
        },
      },
    });
    await sendCommandService.action(msg);
  }
}
