import { getUser, updateUser } from '../../models/user.model';
import { BackCommandService } from './back.command';
import { CommandServiceTool } from '../../types/tools/CommandService.tool';

export class SetProjectCommandService extends CommandServiceTool {
  private readonly backCommandService?: BackCommandService;

  constructor(
    private readonly options: {
      projectName: string;
      backCommandService?: BackCommandService;
    },
  ) {
    super();

    this.backCommandService = options?.backCommandService;
  }

  setOptions(opts) {
    super.setOptions(opts);

    const options = this.getOptions();

    this.backCommandService?.setOptions(options);

    return this;
  }

  public async action(msg): Promise<void> {
    const {
      options: { projectName },
    } = this;
    const {
      chat: { id: chatId },
    } = msg;

    const user = await getUser(chatId);
    const project = user.settings.projects.find(({ name }) => name === projectName);
    if (project == null) {
      throw new Error(`Project "${projectName}" not found`);
    }

    const currentProject = user.settings.currentProjectName;
    await updateUser(user.chatId, {
      settings: {
        ...user.settings,
        currentProjectName: currentProject === projectName ? null : projectName,
      },
    });

    await this.backCommandService?.action(msg);
  }
}
