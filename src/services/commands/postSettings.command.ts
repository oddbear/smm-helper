import { CommandServiceTool } from '../../types/tools/CommandService.tool';
import { getUser, updateUser } from '../../models/user.model';
import { dictionary } from '../../utils/dictionary';
import { CHANGE_LANGUAGE_COMMAND, USE_TAGS_COMMAND, USE_VERBOSE } from '../../utils/constants';
import { User } from '../../types/Schemas.type';
import { Languages } from '../../utils/settings';
import { BackCommandService } from './back.command';
import { EventNames } from '../../types/ClusterContext.type';

export const getUserPostSettingsInlineKeyboard = (user: User) => [
  [
    {
      text: dictionary.settings.post.language(Languages[user.settings.language]),
      callback_data: CHANGE_LANGUAGE_COMMAND,
    },
  ],
  [
    {
      text: dictionary.settings.post.verbose(user.settings.verbose),
      callback_data: USE_VERBOSE,
    },
  ],
  [
    {
      text: dictionary.settings.post.useTags(user.settings.useTags),
      callback_data: USE_TAGS_COMMAND,
    },
  ],
];

export class PostSettingsCommandService extends CommandServiceTool {
  private readonly backCommandService: BackCommandService = new BackCommandService({
    to: dictionary.settings.commandName,
  });

  async action(msg): Promise<void> {
    const options = this.getOptions();
    const { context, telegramBot } = options;

    const user = await getUser(context.chatId);

    const response = await telegramBot.sendMessage(context.chatId, dictionary.settings.post.message, {
      reply_markup: {
        inline_keyboard: getUserPostSettingsInlineKeyboard(user),
      },
    });

    const structureMessageId = user.structureMessageId;
    await updateUser(context.chatId, {
      structureMessageId: null,
    });

    if (structureMessageId != null) {
      try {
        await telegramBot.deleteMessage(context.chatId, structureMessageId.toString());
      } catch (err) {}
    }

    const removeListener = context.addEventListener(EventNames.loopEnd, async () => {
      removeListener();

      await updateUser(context.chatId, {
        $push: {
          removeMessagesIds: response.message_id,
        },
      });
    });

    this.backCommandService.setOptions(options);
    await this.backCommandService.action(msg);
  }
}
