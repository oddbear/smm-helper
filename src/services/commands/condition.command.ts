import { CommandServiceTool } from '../../types/tools/CommandService.tool';
import TelegramBot from 'node-telegram-bot-api';
import { CommandsStructure } from '../../types/CommadsStructure.type';

export class ConditionCommandService extends CommandServiceTool {
  constructor(
    private readonly $if: (msg: TelegramBot.Message) => Promise<boolean>,
    private readonly $else: CommandServiceTool,
  ) {
    super();
  }

  async action(msg: TelegramBot.Message, commandStructure: CommandsStructure[number]): Promise<void> {
    if (!(await this.isTrue(msg))) {
      this.$else.setOptions(this.getOptions());
      await this.$else.action(msg, commandStructure);
    }
  }

  public isTrue(msg: TelegramBot.Message) {
    return this.$if(msg);
  }
}
