import TelegramBot from 'node-telegram-bot-api';
import { ClusterServiceTool } from '../types/tools/ClusterService.tool';
import { CommandsStructureFunction, CommandsStructure } from '../types/CommadsStructure.type';
import { StructureCommandService } from './commands/structure.command';
import { CommandServiceTool } from '../types/tools/CommandService.tool';
import { findCommandStructure, toCommandsStructure } from '../utils/data';
import { ClusterContext } from '../types/ClusterContext.type';

export class CommandsService extends ClusterServiceTool {
  private readonly commandsStructure: CommandsStructureFunction;
  private readonly defaultCommands?: {
    command: string;
    description: string;
  }[];
  private readonly structureCommandService: StructureCommandService = new StructureCommandService();
  private isDefaultCommandsSet: boolean = false;

  constructor(options: {
    structure: CommandsStructureFunction;
    defaultCommands?: {
      command: string;
      description: string;
    }[];
  }) {
    super();

    this.defaultCommands = options.defaultCommands;
    this.commandsStructure = options.structure;
  }

  setOptions(options: { telegramBot: TelegramBot; context: ClusterContext }) {
    super.setOptions(options);

    void this.setDefaultCommands();

    return this;
  }

  async onMessage(message): Promise<boolean> {
    const { context } = this.getOptions();
    context.commandsStructure = await toCommandsStructure(this.commandsStructure, context);

    if (!this.validateMessage(message)) {
      return false;
    }

    const { text: command } = message;

    const commandStructure = await findCommandStructure({
      message,
      command,
      commandsStructure: this.commandsStructure,
      context,
    });
    context.commandStructure = commandStructure ?? context.commandStructure;

    const isAvailable = (await commandStructure?.commandCondition?.isTrue(message)) ?? true;
    if (!isAvailable) {
      await this.actionOnCommandService(commandStructure!.commandCondition!, message, commandStructure!);
    } else {
      if (commandStructure?.commandService != null) {
        const services =
          commandStructure.commandService instanceof Array
            ? commandStructure.commandService
            : [commandStructure.commandService];

        let removeStructure: boolean = false;
        for (const service of services) {
          await this.actionOnCommandService(service, message, commandStructure);
          removeStructure = removeStructure || service.isMessageSender;
        }
      }

      if (commandStructure?.structure != null) {
        if (commandStructure.structureMessage != null) {
          this.structureCommandService.setOptions({
            message: commandStructure.structureMessage,
          });
        }

        await this.actionOnCommandService(this.structureCommandService, message, commandStructure);

        if (commandStructure.structureMessage != null) {
          this.structureCommandService.setOptions({
            message: null,
          });
        }
      }
    }

    return commandStructure != null;
  }

  async onCallbackQuery(query) {
    return this.onMessage({
      ...query.message,
      text: query.data,
    });
  }

  private actionOnCommandService(
    commandService: CommandServiceTool,
    msg: TelegramBot.Message,
    structure: CommandsStructure[number],
  ) {
    commandService.setOptions({
      ...this.getOptions(),
      commandsStructure: this.commandsStructure,
    });
    return commandService.action(msg, structure);
  }

  private validateMessage(msg: TelegramBot.Message): msg is TelegramBot.Message & { text: string } {
    return msg.text != null;
  }

  private async setDefaultCommands(): Promise<boolean> {
    const { defaultCommands, isDefaultCommandsSet } = this;
    if (defaultCommands == null || isDefaultCommandsSet) {
      return false;
    }

    this.isDefaultCommandsSet = true;

    return true;
  }
}
