import { ClusterServiceTool } from '../types/tools/ClusterService.tool';
import { InputHandlerServiceTool } from '../types/tools/InputHandlerService.tool';
import { InputHandlerOptions } from '../types/InputHandler.type';
import { getUser, updateUser } from '../models/user.model';

export class InputHandlerService extends ClusterServiceTool {
  constructor(private readonly inputHandlerOptions: InputHandlerOptions) {
    super();
  }

  async onMessage(msg): Promise<boolean> {
    const { inputHandlerOptions } = this;

    const options = this.getOptions();

    const user = await getUser(msg.chat.id);
    const { inputHandler } = user;

    if (inputHandler == null) {
      return false;
    }

    const service = inputHandlerOptions[inputHandler.command] as InputHandlerServiceTool | undefined;
    if (service == null) {
      throw new Error(`InputHandlerService: no service for command ${inputHandler.command}`);
    }

    service.setOptions({
      ...options,
      properties: inputHandler.options,
    });
    await service.activate(msg);

    return true;
  }

  async onStopPropagation({ message, query }) {
    const chatId = message?.chat.id ?? query?.message.chat.id;
    if (chatId == null) {
      return;
    }

    const user = await getUser(chatId);
    if (user.inputHandler != null) {
      await updateUser(chatId, {
        inputHandler: null,
      });
    }
  }
}
