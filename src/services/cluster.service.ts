import TelegramBot from 'node-telegram-bot-api';
import { createTelegramBot } from '../utils/telegram';
import { ClusterServiceTool } from '../types/tools/ClusterService.tool';
import { ClusterContext, EventNames } from '../types/ClusterContext.type';
import { updateUser } from '../models/user.model';

export class ClusterService {
  private readonly telegramBot: TelegramBot;
  private readonly events: {
    [key in EventNames]: { callback: (context: ClusterContext) => unknown; chatId: number }[];
  } = {
    [EventNames.loopEnd]: [],
  };
  private readonly services: ClusterServiceTool[] = [];

  constructor(options: { services: ClusterServiceTool[] }) {
    this.dispatchCallbackQuery = this.dispatchCallbackQuery.bind(this);

    const telegramBot = (this.telegramBot = createTelegramBot());

    telegramBot.on('error', (err) => {
      console.log(`error: ${err.message}`);
    });
    telegramBot.on('polling_error', (err) => {
      console.log(`polling_error: ${err.message}`);
    });

    telegramBot.on(
      'callback_query',
      this.triggerServicesBind<TelegramBot.CallbackQuery>({
        getIdCallback: (query) => query.message?.chat.id ?? 0,
        actionCallback: async (service, query) => {
          return service.onCallbackQuery?.(query) ?? false;
        },
        stopPropagationCallback: async (service, query) => {
          await service.onStopPropagation?.({
            query,
          });
        },
      }),
    );
    telegramBot.on(
      'message',
      this.triggerServicesBind<TelegramBot.Message>({
        getIdCallback: (query) => query.chat.id,
        actionCallback: async (service, message) => {
          return service.onMessage?.(message) ?? false;
        },
        stopPropagationCallback: async (service, message) => {
          await service.onStopPropagation?.({
            message,
          });
        },
      }),
    );
    telegramBot.on(
      'pre_checkout_query',
      this.triggerServicesBind<TelegramBot.PreCheckoutQuery>({
        getIdCallback: () => {
          return -1;
        },
        actionCallback: async (service, preCheckoutQuery) => {
          return service.onPreCheckoutQuery?.(preCheckoutQuery) ?? false;
        },
        stopPropagationCallback: async (service, preCheckoutQuery) => {
          await service.onStopPropagation?.({
            preCheckoutQuery,
          });
        },
      }),
    );
    telegramBot.on(
      'successful_payment',
      this.triggerServicesBind<TelegramBot.Message>({
        getIdCallback: (payment) => {
          return payment.chat.id;
        },
        actionCallback: async (service, payment) => {
          return service.onSuccessfulPayment?.(payment) ?? false;
        },
        stopPropagationCallback: async (service, payment) => {
          await service.onStopPropagation?.({
            payment,
          });
        },
      }),
    );

    for (const service of options.services) {
      this.addService(service);
    }
  }

  private async dispatchCallbackQuery(context: ClusterContext, query: TelegramBot.CallbackQuery) {
    const chatId = query.message?.chat.id;
    if (chatId == null) {
      return;
    }

    await updateUser(chatId, {
      isWaitingForResponse: false,
    });

    let isStopPropagate = false;
    for (const service of this.services) {
      service.setOptions({ context, telegramBot: this.telegramBot });

      if (isStopPropagate) {
        await service.onStopPropagation?.({
          query,
        });
      } else if ((await service.onCallbackQuery?.(query)) === true) {
        isStopPropagate = true;
      }
    }

    this.triggerEvent(chatId, EventNames.loopEnd, [context]);
  }

  private triggerServicesBind<
    T extends TelegramBot.Message | TelegramBot.CallbackQuery | TelegramBot.PreCheckoutQuery,
  >(options: {
    getIdCallback: (data: T) => number;
    actionCallback: (service: ClusterServiceTool, data: T) => Promise<boolean>;
    stopPropagationCallback: (service: ClusterServiceTool, data: T) => Promise<void>;
  }): (data: T) => Promise<void> {
    let contextId: number = 0;

    return async (data: T) => {
      const chatId = options.getIdCallback(data);

      const context: ClusterContext = {
        contextId: ++contextId > 9999 ? (contextId = 0) : contextId,
        chatId,

        commandsStructure: null,
        commandStructure: null,

        dispatchCallbackQuery: this.dispatchCallbackQuery,
        addEventListener: (eventName, callback) => this.addEventListener(chatId, eventName, callback),

        stopPropagation: false,
        dontRemoveMessage: false,
      };

      let isStopPropagate = false;
      for (const service of this.services) {
        if (context.stopPropagation) {
          break;
        }

        service.setOptions({ context, telegramBot: this.telegramBot });

        if (isStopPropagate) {
          await options.stopPropagationCallback(service, data);
        } else if (await options.actionCallback(service, data)) {
          isStopPropagate = true;
        }
      }

      this.triggerEvent(chatId, EventNames.loopEnd, [context]);
    };
  }

  private triggerEvent(targetChatId: number, eventName: EventNames, args: [ClusterContext]): void {
    const callbacks = [...this.events[eventName]];
    for (const { callback, chatId } of callbacks) {
      if (targetChatId === chatId) {
        callback(...args);
      }
    }
  }

  private addEventListener(
    chatId: number,
    eventName: EventNames,
    callback: (context: ClusterContext) => unknown,
  ): () => void {
    const eventInfo = {
      callback,
      chatId,
    };

    this.events[eventName].push(eventInfo);
    return () => {
      const index = this.events[eventName].indexOf(eventInfo);
      this.events[eventName].splice(index, 1);
    };
  }

  private addService(service: ClusterServiceTool): void {
    this.services.push(service);
  }
}
