import { SendCommandService } from './commands/send.command';
import { dictionary } from '../utils/dictionary';
import { ClusterServiceTool } from '../types/tools/ClusterService.tool';
import { addAnalyticsEvent } from '../utils/analytics';
import TelegramBot from 'node-telegram-bot-api';

export class NotFoundService extends ClusterServiceTool {
  private readonly sendCommandService: SendCommandService = new SendCommandService();

  public async onMessage(msg: TelegramBot.Message) {
    void addAnalyticsEvent('notFound');

    const { text } = msg;
    if (text == null) {
      return false;
    }

    this.sendCommandService.setOptions({
      ...this.getOptions(),
      message: {
        text: dictionary.notFound.message(text),
        parse_mode: 'Markdown',
        reply_markup: {
          inline_keyboard: dictionary.notFound.options.map((option) => [{ text: option, callback_data: option }]),
        },
      },
    });
    await this.sendCommandService.action(msg);

    return true;
  }
}
