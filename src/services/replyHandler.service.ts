import { ClusterServiceTool } from '../types/tools/ClusterService.tool';
import { addAnalyticsEvent } from '../utils/analytics';
import TelegramBot from 'node-telegram-bot-api';
import { HASH_SETTINGS_REGEXP } from '../utils/constants';
import { parseUserHash } from '../utils/hash';
import { getUser } from '../models/user.model';
import { dictionary } from '../utils/dictionary';
import { GenerateInputHandlerService } from './inputHandlers/generate.inputHandler';
import { CommandsStructureFunction } from '../types/CommadsStructure.type';
import { toCommandsStructure } from '../utils/data';

export class ReplyHandlerService extends ClusterServiceTool {
  private readonly generateInputHandlerService: GenerateInputHandlerService;

  constructor(
    private readonly backCommandMessage: string,
    private readonly commandServiceStructure: CommandsStructureFunction,
  ) {
    super();

    this.generateInputHandlerService = new GenerateInputHandlerService(backCommandMessage);
  }

  public async onMessage(message: TelegramBot.Message) {
    void addAnalyticsEvent('fulfill');

    if (
      message.text == null ||
      message.reply_to_message?.text == null ||
      message.reply_to_message.from?.is_bot !== true
    ) {
      return false;
    }

    const {
      chat: { id: chatId },
      text,
      reply_to_message: { text: replyText },
    } = message;

    const extractedSettings = HASH_SETTINGS_REGEXP.exec(replyText)?.[1];
    if (extractedSettings == null) {
      return false;
    }

    const user = await getUser(chatId);
    const parsedSettings = parseUserHash(user.settings, extractedSettings);

    const settingsText = dictionary.generate.settingsInfoWithoutMarkup(parsedSettings);
    const settingsInfoRegExp = new RegExp(`${settingsText.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&')}$`);
    if (settingsInfoRegExp.exec(replyText) == null) {
      return false;
    }

    const replyTextWithoutSettings = replyText.replace(settingsText, '').trim();

    const options = this.getOptions();
    const commandsStructure = await toCommandsStructure(this.commandServiceStructure, options.context);
    this.generateInputHandlerService.setOptions({
      ...options,
      commandsStructure,
      disableRepeatButton: true,
      customSettings: parsedSettings,
    });
    await this.generateInputHandlerService.activate({
      ...message,
      text: dictionary.fulfillCompletion.buildMessage(replyTextWithoutSettings, text),
    });

    return true;
  }
}
