export const MAX_PROJECTS_LENGTH = 3;
export const MAX_SYMBOLS_IN_PROJECT_NAME = 30;
export const MAX_WORDS_IN_PROJECT_NAME = 5;
export const MAX_SYMBOLS_IN_PROJECT_DESCRIPTION = 3000;
export const DELAY_BEFORE_MENU_UPDATED_AFTER_GENERATION = 3000;
export const PRICE_FOR_CREDITS = {
  regular: 1,
  verbose: 1,
  image: 2,
};

export const DELETE_KEY_WORD = 'я уверен';
export const ADD_IMAGES_COMMAND = 'add-images';
export const REPEAT_IMAGE_COMMAND = 'repeat-image';
export const REPEAT_COMPLETION_COMMAND = 'repeat-completion';
export const FULFILL_COMPLETION_COMMAND = 'fulfill-completion';
export const COMPLAIN_COMMAND = 'complain';
export const CHANGE_LANGUAGE_COMMAND = 'change-language';
export const USE_VERBOSE = 'use-verbose';
export const USE_TAGS_COMMAND = 'use-tags';
export const MORE_COMMAND = 'more';
export const HIDE_ERROR_COMMAND = 'hide-error';

export const INPUT_HANDLER_COMMANDS = [
  'generate',
  'projectAdd',
  'projectDescription',
  'projectDeleteConfirmation',
] as const;
export type InputHandlerCommand = typeof INPUT_HANDLER_COMMANDS[number];

export const TONES = {
  humorous: 'set-tone-humorous',
  informative: 'set-tone-informative',
  formal: 'set-tone-formal',
  positive: 'set-tone-positive',
  sad: 'set-tone-sad',
  negative: 'set-tone-negative',
};

export const FORMATS = {
  'facebook post': 'set-format-social-media',
  'blog post': 'set-format-description',
  'long text': 'set-format-long-text',
  email: 'set-format-email',
  faq: 'set-format-faq',
  idea: 'set-format-idea',
  improve: 'set-format-improve',
  expand: 'set-format-expand',
  shorten: 'set-format-shorten',
  rephrase: 'set-format-rephrase',
  'question and answer': 'set-question-and-answer',
  list: 'set-list',
  comparison: 'set-comparison',
  'how-to guide': 'set-how-to-guide',
  'top 10/5/3 list': 'set-top-list',
  news: 'set-news',
  testimonial: 'set-testimonial',
  'story/narrative': 'set-story',
};

export const ANALYTICS_EVENTS = [
  'join',
  'leave',
  'settingsClick',
  'projectsSettingsClick',
  'projectSettingsClick',
  'projectCreateClick',
  'projectCreate',
  'projectSetClick',
  'projectUnsetClick',
  'projectEditClick',
  'projectEdit',
  'projectDeleteClick',
  'projectDeleteConfirm',
  'postSettingsClick',
  'postChangeLanguage',
  'postUseTags',
  'postUseVerbose',
  'helpClick',
  'creditsClick',
  'creditsBuyClick',
  'preCheckout',
  'purchase',
  'generateClick',
  'generateCancel',
  'preGenerate',
  'generate',
  'repeat',
  'complain',
  'fulfillInfo',
  'fulfill',
  'preGenerateImage',
  'generateImage',
  'repeatImage',
  'notEnoughCredits',
  'preventCommand',
  'notFound',
] as const;
export type AnalyticsEvent = typeof ANALYTICS_EVENTS[number];

export const HASH_SETTINGS_BREAKS = ['〉', '〈'];
export const HASH_SETTINGS_REGEXP = new RegExp(`${HASH_SETTINGS_BREAKS[0]}(.+)${HASH_SETTINGS_BREAKS[1]}`);

export const GOODS = [
  {
    title: '10 кредитов',
    label: '10 кред. (1$)',
    amount: 1 * 100,
    creditsCount: 10,
  },
  {
    title: '50 кредитов',
    label: '50 кред. (5$)',
    amount: 5 * 100,
    creditsCount: 50,
  },
  {
    title: '250 кредитов',
    label: '250 кред. (22$)',
    amount: 22 * 100,
    creditsCount: 250,
  },
  {
    title: '1000 кредитов',
    label: '1000 кред. (85$)',
    amount: 85 * 100,
    creditsCount: 1000,
  },
  {
    title: '2500 кредитов',
    label: '2500 кред. (200$)',
    amount: 200 * 100,
    creditsCount: 2500,
  },
  {
    title: '10000 кредитов',
    label: '10000 кред. (720$)',
    amount: 720 * 100,
    creditsCount: 10000,
  },
];
