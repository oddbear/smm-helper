import { CommandsStructureFunction, CommandsStructure } from '../types/CommadsStructure.type';
import TelegramBot, { InlineKeyboardButton } from 'node-telegram-bot-api';
import { MessageType, UnionMessageType } from '../types/Message.type';
import { ClusterContext } from '../types/ClusterContext.type';
import { dictionary } from './dictionary';
import { SendCommandService } from '../services/commands/send.command';
import { MORE_COMMAND, HASH_SETTINGS_REGEXP } from './constants';
import { User } from '../types/Schemas.type';
import { getAnalytics } from '../models/analytics.model';
import { parseUserHash } from './hash';

export const MAX_ITEMS_IN_ROW = 3;
export const MAX_ROWS_IN_MESSAGE = 2;

export const findCommandStructure = async (options: {
  message: TelegramBot.Message;
  command: string;
  commandsStructure: CommandsStructureFunction;
  context: ClusterContext;
}): Promise<CommandsStructure[number] | null> => {
  const inside = async (
    command: string,
    commandsStructure: CommandsStructure,
    conditionCommandService?: CommandsStructure[number],
  ): Promise<CommandsStructure[number] | null> => {
    if (!(commandsStructure instanceof Array)) {
      console.error(
        `commandsStructure by command "${command}" is not an array: ${JSON.stringify(commandsStructure, null, 4)}`,
      );
    }
    for (const commandStructure of commandsStructure) {
      const { commandName, commandCondition, structure } = commandStructure;
      if (commandName === command) {
        return conditionCommandService ?? commandStructure;
      }

      if (structure != null) {
        let isConditionPassed = true;
        if (conditionCommandService == null && commandCondition != null) {
          isConditionPassed = await commandCondition.isTrue(options.message);
        }

        const preparedStructure = await toCommandsStructure(structure, options.context);
        const result = await inside(
          command,
          preparedStructure,
          isConditionPassed ? conditionCommandService : commandStructure,
        );
        if (result != null) {
          return result;
        }
      }
    }

    return null;
  };

  return inside(options.command, await toCommandsStructure(options.commandsStructure, options.context));
};

export const extractIdFromMessage = (text: string): string | undefined => {
  return /1\.([^\n]+)/.exec(text)?.[1]?.trim();
};

export const toMessageObject = async (message: UnionMessageType, context?: ClusterContext): Promise<MessageType> => {
  if (typeof message === 'string') {
    return {
      text: message,
    };
  }

  if (typeof message === 'function') {
    if (context == null) {
      throw new Error(`Context is required for message function ${message.toString()}`);
    }

    return toMessageObject(await message(context), context);
  }

  const inlineButtons: TelegramBot.InlineKeyboardButton[][] = [];
  for (const button of message.reply_markup?.inline_keyboard ?? []) {
    const inlineButton: TelegramBot.InlineKeyboardButton[] = [];
    for (const buttonItem of button) {
      inlineButton.push({
        callback_data: buttonItem.callback_data,
        text: buttonItem.text,
        url: buttonItem.url,
      });
    }

    inlineButtons.push(inlineButton);
  }

  return {
    ...message,
    text: message.text,
    reply_markup:
      message.reply_markup != null
        ? {
            ...message.reply_markup,
            inline_keyboard: inlineButtons,
          }
        : undefined,
  };
};

export const toCommandsStructure = async (
  commandsStructure: CommandsStructureFunction,
  context: ClusterContext,
): Promise<CommandsStructure> => {
  if (typeof commandsStructure === 'function') {
    return commandsStructure(context);
  }

  return commandsStructure;
};

export const asyncReduce = async <T, R>(
  array: T[],
  callback: (acc: R, item: T, index: number, array: T[]) => Promise<R>,
  initialValue: R,
): Promise<R> => {
  let acc = initialValue;
  for (let i = 0; i < array.length; i++) {
    acc = await callback(acc, array[i], i, array);
  }

  return acc;
};

export const toOnlyText = (text: string): string => {
  return /([\wа-яА-Я]+)/.exec(text)?.[1] ?? '';
};

export const buildStructureFromCategories = <T extends string>(
  categories: { [key in T]: { commandName: string } },
  prepareCallback?: (key: string) => Partial<CommandsStructure[number]>,
  postfix?: string,
) => {
  return Object.entries(categories).map(([key, value]) => {
    const v = value as { commandName: string };
    return {
      commandName: postfix == null ? v.commandName : `${v.commandName} ${postfix}`,
      search: v.commandName,
      ...prepareCallback?.(key),
    };
  });
};

export const getFormattedStructureList = (
  list: Omit<CommandsStructure[number], 'row' | 'hidden'>[],
  emoji: string,
  limit = MAX_ITEMS_IN_ROW * MAX_ROWS_IN_MESSAGE,
) => {
  const isVertical = list.length <= MAX_ITEMS_IN_ROW;

  const results: CommandsStructureFunction = list.map((item, index) => {
    return {
      ...item,
      commandName: item.commandName,
      row: isVertical ? index : Math.floor(index / MAX_ITEMS_IN_ROW),
      hidden: index > limit - 1,
    };
  });

  const placeholder = dictionary.placeholders.more(emoji);
  if (list.length > limit) {
    results.push({
      row: 5,
      commandName: placeholder.commandName,
      commandService: new SendCommandService({
        message: {
          text: placeholder.message,
          reply_markup: {
            inline_keyboard: getShowMoreInlineKeyboard(
              list.map((item) => {
                if (typeof item.commandName === 'function') {
                  throw new Error('Function command name is not supported for inline_keyboard');
                }

                return item.commandName;
              }),
            ),
          },
        },
      }),
    });
  }

  return results;
};

export const getShowMoreInlineKeyboard = (data: string[]) => {
  return data
    .sort((a, b) => toOnlyText(a).localeCompare(toOnlyText(b)))
    .reduce<InlineKeyboardButton[][]>((acc, commandName, index) => {
      if (index % 2 === 0) {
        acc.push([]);
      }
      acc[acc.length - 1].push({
        text: commandName,
        callback_data: commandName,
      });
      return acc;
    }, []);
};

export const getFormattedInlineKeyboard = (
  data: {
    text: string;
    command: string;
    callback_data: string;
  }[],
  emoji: string,
  maxLength = 6,
) => {
  const isShowMoreButton = data.length > maxLength;
  const buttons = isShowMoreButton ? data.slice(0, maxLength) : data;

  const inlineKeyboard = buttons.reduce<InlineKeyboardButton[][]>((acc, button, index) => {
    if (index % 3 === 0) {
      acc.push([]);
    }
    acc[acc.length - 1].push({
      text: button.text,
      callback_data: button.callback_data,
    });
    return acc;
  }, []);

  if (isShowMoreButton) {
    inlineKeyboard.push([
      {
        text: dictionary.placeholders.more(emoji).commandName,
        callback_data: `${MORE_COMMAND}-${emoji}`,
      },
    ]);
  }

  return inlineKeyboard;
};

export const getSortedByRatingInlineKeyboard = async <T extends { [key: string]: string }>(
  user: User,
  data: T,
  propertyName: keyof User['settings'],
  maxLength: number = 6,
) => {
  const analytics = await getAnalytics();
  const preGenerateEvent = analytics.events.find(({ eventName }) => eventName === 'preGenerate');
  if (preGenerateEvent?.values == null) {
    throw new Error('Generate event not found');
  }
  const valuesMap = preGenerateEvent.values.reduce<Map<string, number>>((acc, { valueName, count }) => {
    if (valueName.includes(propertyName)) {
      acc.set(valueName.split('-')[1], count);
    }
    return acc;
  }, new Map());

  const entries = Object.entries(data).sort(([a], [b]) => {
    const aAnalytics = valuesMap.get(a);
    const bAnalytics = valuesMap.get(b);

    if (aAnalytics == null || bAnalytics == null) {
      return 0;
    }

    return aAnalytics === bAnalytics ? 0 : aAnalytics > bAnalytics ? -1 : 1;
  });

  const propertyValue = user.settings[propertyName];
  const propertyIndex = entries.findIndex(([key]) => key === propertyValue);

  return getFormattedInlineKeyboard(
    entries.map(([value, callbackData]) => ({
      // eslint-disable-next-line @typescript-eslint/no-unsafe-call,@typescript-eslint/no-unsafe-member-access
      text: dictionary.generate[propertyName][value](propertyValue === value),
      command: value,
      callback_data: callbackData,
    })),
    // eslint-disable-next-line @typescript-eslint/no-unsafe-call,@typescript-eslint/no-unsafe-member-access
    dictionary.generate[propertyName].emoji,
    maxLength < propertyIndex + 1 ? Infinity : maxLength,
  );
};

export const parseUserSettingsFromText = (userSettings: User['settings'], text: string): User['settings'] => {
  let userSettingsHash = HASH_SETTINGS_REGEXP.exec(text)?.[1];
  if (userSettingsHash == null) {
    userSettingsHash = '';
  }
  const parsedUserHash = parseUserHash(userSettings, userSettingsHash);

  return {
    ...userSettings,
    ...parsedUserHash,
  };
};
