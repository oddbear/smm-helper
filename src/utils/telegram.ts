import TelegramBot, { ParseMode } from 'node-telegram-bot-api';
import { config } from '../config';
import axios from 'axios';
import { dictionary } from './dictionary';
import { HIDE_ERROR_COMMAND } from './constants';

export const createTelegramBot = (): TelegramBot => {
  return new TelegramBot(config.telegram.token, {
    polling: true,
  });
};

export const escapeCharactersForTelegram = (text: string) => {
  return text.replace(/([_\-*[\]()~`>#+=|{}\.!])/g, '\\$1');
};

export const sendInvoice = (
  chatId: number,
  title: string,
  description: string,
  payload: string,
  startParameter: string,
  currency: string,
  prices: { label: string; amount: number }[],
) => {
  const data = {
    chat_id: chatId.toString(),
    title,
    description,
    payload,
    provider_token: config.telegram.paymentToken,
    start_parameter: startParameter,
    currency,
    prices,
  };

  return axios.post<{
    result: {
      message_id: string;
    };
  }>(`https://api.telegram.org/bot${config.telegram.token}/sendInvoice`, data);
};

export const handleError = async (
  telegramBot: TelegramBot,
  options: {
    chatId: number;
    text?: string;
    replyId?: number;
  },
  err: Error,
) => {
  const messageOptions = {
    parse_mode: 'MarkdownV2' as ParseMode,
    reply_markup: {
      inline_keyboard: [
        [
          {
            text: dictionary.internalError.hide,
            callback_data: HIDE_ERROR_COMMAND,
          },
        ],
      ],
    },
  };

  if (options.replyId != null) {
    await telegramBot
      .editMessageText(dictionary.internalError.message(err.message, options.text), {
        ...messageOptions,
        chat_id: options.chatId,
        message_id: options.replyId,
      })
      .catch(async () => {
        options.replyId = (
          await telegramBot.sendMessage(
            options.chatId,
            dictionary.internalError.message(err.message, options.text),
            messageOptions,
          )
        ).message_id;
      });
  } else {
    options.replyId = (
      await telegramBot.sendMessage(
        options.chatId,
        dictionary.internalError.message(err.message, options.text),
        messageOptions,
      )
    ).message_id;
  }

  // let removeMessageId: number;
  // if (typeof response !== 'boolean') {
  //   removeMessageId = response.message_id;
  // } else {
  //   if (replyId == null) {
  //     return;
  //   }
  //
  //   removeMessageId = replyId;
  // }
  //
  // await updateUser(chatId, {
  //   $push: {
  //     removeMessagesIds: removeMessageId,
  //   },
  // });

  console.error(err);

  return options.replyId;
};
