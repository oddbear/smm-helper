import { CommandsStructureFunction, CommandsStructure } from '../types/CommadsStructure.type';
import { dictionary } from './dictionary';
import { SendCommandService } from '../services/commands/send.command';
import { BackCommandService } from '../services/commands/back.command';
import { InputHandlerCommandService } from '../services/commands/inputHandler.command';
import { SetProjectCommandService } from '../services/commands/setProject.command';
import { getUser } from '../models/user.model';
import { ProjectDescriptionInputHandlerService } from '../services/inputHandlers/projectDescription.inputHandler';
import { ProjectDeleteInputHandlerService } from '../services/inputHandlers/projectDelete.inputHandler';
import { AggregatorCommandService } from '../services/commands/aggregator.command';
import { GenerateInputHandlerService } from '../services/inputHandlers/generate.inputHandler';
import { PostSettingsCommandService } from '../services/commands/postSettings.command';
import { GOODS, MAX_PROJECTS_LENGTH } from './constants';
import { FormatSettingsCommand } from '../services/commands/formatSettings.command';
import { ToneSettingsCommand } from '../services/commands/toneSettings.command';
import { PurchaseCommandService } from '../services/commands/purchaseCommandService';
import { AnalyticsCommandService } from '../services/commands/analytics.command';
import { InputHandlerOptions } from '../types/InputHandler.type';
import { ProjectAddInputHandlerService } from '../services/inputHandlers/projectAdd.inputHandler';
import { ClusterContext } from '../types/ClusterContext.type';
import { ResendStructureCommand } from '../services/commands/resendStructure.command';

export const getBuildServices = async (): Promise<{
  commandServiceStructure: CommandsStructureFunction;
  inputHandlerOptions: InputHandlerOptions;
}> => {
  const commandServiceStructure = [
    {
      row: 0,
      commandName: dictionary.startBot,
      commandService: new SendCommandService({
        saveMessage: true,
        message: {
          text: dictionary.welcome.text,
        },
      }),
      structureMessage: dictionary.backBtnMessage,
      structure: [
        {
          row: 0,
          commandName: dictionary.generate.commandName,
          commandService: new AggregatorCommandService([
            new InputHandlerCommandService('generate'),
            new ToneSettingsCommand(),
            new FormatSettingsCommand(),
            new ResendStructureCommand(),
            new AnalyticsCommandService('generateClick'),
          ]),
          structureMessage: dictionary.generate.message,
          structure: [
            {
              row: 5,
              commandName: dictionary.backBtn,
              commandService: new BackCommandService({
                to: dictionary.startBot,
                message: dictionary.backBtnMessage,
              }),
            },
          ],
        },
        {
          row: 0,
          commandName: dictionary.settings.commandName,
          commandService: new AnalyticsCommandService('settingsClick'),
          search: dictionary.settings.commandName,
          structure: [
            {
              row: 0,
              commandName: dictionary.settings.projects.commandName,
              commandService: new AnalyticsCommandService('projectsSettingsClick'),
              search: dictionary.settings.projects.commandName,
              structure: async (context) => {
                const { chatId } = context;
                const user = await getUser(chatId);

                const projects = user.settings.projects;
                const currentProjectName = user.settings.currentProjectName;

                const result: CommandsStructure = projects.map((project, i) => {
                  const isDefault = currentProjectName === project.name;

                  return {
                    row: i % 2,
                    commandName: dictionary.settings.projects.project.commandName(project.name, isDefault),
                    commandService: new AnalyticsCommandService('projectSettingsClick', `project-${i + 1}`),
                    search: dictionary.settings.projects.project.commandName(project.name, isDefault),
                    structureMessage: `${dictionary.placeholders.choose(
                      dictionary.settings.projects.project.structureName,
                    )}\n\n${dictionary.settings.projects.project.projectInfo(
                      project.name,
                      isDefault,
                      project.description,
                    )}`,
                    structure: [
                      {
                        row: 0,
                        commandName: dictionary.settings.projects.project.edit.commandName(i),
                        commandService: new AggregatorCommandService([
                          new InputHandlerCommandService('projectDescription', {
                            project,
                          }),
                          new AnalyticsCommandService('projectEditClick', `project-${i + 1}`),
                        ]),
                        structureMessage: dictionary.settings.projects.project.edit.message,
                        structure: [
                          {
                            row: 5,
                            commandName: dictionary.settings.projects.backBtn,
                            commandService: new BackCommandService({
                              to: dictionary.settings.projects.commandName,
                            }),
                          },
                        ],
                      },
                      {
                        row: 1,
                        commandName: isDefault
                          ? dictionary.settings.projects.project.unset.commandName(i)
                          : dictionary.settings.projects.project.set.commandName(i),
                        commandService: new AggregatorCommandService([
                          new SetProjectCommandService({
                            projectName: project.name,
                            backCommandService: new BackCommandService({
                              to: dictionary.settings.projects.commandName,
                              message: isDefault
                                ? `${dictionary.placeholders.choose(dictionary.settings.projects.commandName)}\n\n${
                                    dictionary.settings.projects.project.unset.success
                                  }`
                                : `${dictionary.placeholders.choose(dictionary.settings.projects.commandName)}\n\n${
                                    dictionary.settings.projects.project.set.success
                                  }`,
                            }),
                          }),
                          new AnalyticsCommandService(
                            isDefault ? 'projectUnsetClick' : 'projectSetClick',
                            `project-${i + 1}`,
                          ),
                        ]),
                      },
                      {
                        row: 1,
                        commandName: dictionary.settings.projects.project.delete.commandName(i),
                        commandService: new AggregatorCommandService([
                          new InputHandlerCommandService('projectDeleteConfirmation', {
                            project,
                          }),
                          new AnalyticsCommandService('projectDeleteClick', `project-${i + 1}`),
                        ]),
                        structureMessage: dictionary.settings.projects.project.delete.message,
                        structure: [
                          {
                            row: 5,
                            commandName: dictionary.settings.projects.backBtn,
                            commandService: new BackCommandService({
                              to: dictionary.settings.projects.commandName,
                            }),
                          },
                        ],
                      },
                      {
                        row: 5,
                        commandName: dictionary.settings.projects.backBtn,
                        commandService: new BackCommandService({
                          to: dictionary.settings.projects.commandName,
                        }),
                      },
                    ],
                  };
                });

                if (projects.length < MAX_PROJECTS_LENGTH) {
                  result.push({
                    row: 1,
                    commandName: dictionary.settings.projects.add.commandName,
                    structureMessage: dictionary.settings.projects.add.message,
                    structure: [
                      {
                        row: 5,
                        commandName: dictionary.settings.projects.backBtn,
                        commandService: new BackCommandService({
                          to: dictionary.settings.projects.commandName,
                        }),
                      },
                    ],
                    commandService: new AggregatorCommandService([
                      new InputHandlerCommandService('projectAdd'),
                      new AnalyticsCommandService('projectCreateClick', `project-${projects.length + 1}`),
                    ]),
                  });
                }

                result.push({
                  row: 5,
                  commandName: dictionary.settings.backBtn,
                  commandService: new BackCommandService({
                    to: dictionary.settings.commandName,
                  }),
                });

                return result;
              },
            },
            {
              row: 1,
              commandName: dictionary.settings.post.commandName,
              commandService: new AggregatorCommandService([
                new PostSettingsCommandService(),
                new AnalyticsCommandService('postSettingsClick'),
              ]),
            },
            {
              row: 5,
              commandName: dictionary.backBtn,
              commandService: new BackCommandService({
                to: dictionary.startBot,
                message: dictionary.backBtnMessage,
              }),
            },
          ],
        },
        {
          row: 1,
          commandName: dictionary.credits.commandName,
          commandService: new AnalyticsCommandService('creditsClick'),
          search: dictionary.credits.commandName,
          structureMessage: async (context: ClusterContext) => {
            const user = await getUser(context.chatId);
            return dictionary.credits.balance(user.credits);
          },
          structure: [
            ...GOODS.map((good, i) => ({
              row: Math.floor(i / Math.floor(GOODS.length / 3)),
              commandName: good.label,
              commandService: new AggregatorCommandService([
                new PurchaseCommandService(good),
                new AnalyticsCommandService('creditsBuyClick', good.label),
              ]),
            })),
            {
              row: 5,
              commandName: dictionary.backBtn,
              commandService: new BackCommandService({
                to: dictionary.startBot,
                message: dictionary.backBtnMessage,
              }),
            },
          ],
        },
        {
          row: 1,
          commandName: dictionary.help.commandName,
          commandService: new AggregatorCommandService([
            new SendCommandService({
              message: {
                parse_mode: 'MarkdownV2',
                text: dictionary.help.message,
              },
            }),
            new AnalyticsCommandService('helpClick'),
          ]),
        },
      ],
    },
  ];

  const inputHandlerOptions: InputHandlerOptions = {
    generate: new GenerateInputHandlerService(`${dictionary.generate.backMessage}\n\n${dictionary.backBtnMessage}`),
    projectAdd: new ProjectAddInputHandlerService(),
    projectDescription: new ProjectDescriptionInputHandlerService(),
    projectDeleteConfirmation: new ProjectDeleteInputHandlerService(),
  };

  return {
    inputHandlerOptions,
    commandServiceStructure,
  };
};
