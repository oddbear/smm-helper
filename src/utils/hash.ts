import { User } from '../types/Schemas.type';
import { Languages } from './settings';
import { FORMATS, TONES } from './constants';
import pickBy from 'lodash/pickBy';

export const buildHash = (options: { [key: string]: number | boolean }): string => {
  // options should be parsed with Object.keys and sorted by alphabet
  const keys = Object.keys(options);
  const sortedKeys = keys.sort((a, b) => a.localeCompare(b));

  // then get values from sortedKeys as array
  const values = sortedKeys.map((key) => options[key]);

  // each value should be converted with function "convertValueToHash" and added to summary string in order
  return values.reduce((acc, value) => {
    return acc + convertValueToHash(value);
  }, '');
};

export const parseHash = (keys: string[], hash: string): { [key: string]: number | boolean } => {
  const result = {};
  const letters = Object.keys(hash);

  const sortedKeys = keys.sort((a, b) => a.localeCompare(b));

  letters.forEach((key, index) => {
    result[sortedKeys[index]] = convertHashToValue(hash[index]);
  });
  return result;
};

// dictionary object should start from booleans and then numbers that have values from 0 to 9
// each value should have alphabetical or numerical designation
const dictionary: { [key: string]: string } = {
  true: 'a',
  false: 'b',
  '-1': 'Z',
  0: 'c',
  1: 'd',
  2: 'e',
  3: 'f',
  4: 'g',
  5: 'h',
  6: 'i',
  7: 'j',
  8: 'k',
  9: 'l',
  10: 'm',
  11: 'n',
  12: 'o',
  13: 'p',
  14: 'q',
  15: 'r',
  16: 's',
  17: 't',
  18: 'u',
  19: 'v',
  20: 'w',
  21: 'x',
  22: 'y',
  23: 'z',
};
export const reversedDictionary: { [key: string]: string } = Object.entries(dictionary).reduce((acc, [key, value]) => {
  acc[value] = key;
  return acc;
}, {});

export const convertValueToHash = (value: number | boolean): string => {
  const stringValue = value.toString();
  if (!(stringValue in dictionary)) {
    throw new Error(`Value ${value.toString()} is not in dictionary`);
  }

  return dictionary[stringValue];
};

export const convertHashToValue = (letter: string): number | boolean => {
  if (!(letter in reversedDictionary)) {
    throw new Error(`Letter ${letter} is not in reversedDictionary`);
  }

  const stringValue = reversedDictionary[letter];
  const numberValue = Number(stringValue);
  return isNaN(numberValue) ? stringValue === 'true' : numberValue;
};

export const buildUserHash = (userSettings: User['settings']): string => {
  const { language, verbose, useTags, projects, currentProjectName, tone, format } = userSettings;

  const languageIndex = Object.keys(Languages).findIndex((lang) => lang === language);
  const projectIndex =
    currentProjectName == null ? -1 : projects.findIndex((project) => project.name === currentProjectName);
  const toneIndex = tone == null ? -1 : Object.keys(TONES).findIndex((toneName) => toneName === tone);
  const formatIndex = format == null ? -1 : Object.keys(FORMATS).findIndex((formatName) => formatName === format);

  return buildHash({
    languageIndex,
    verbose,
    useTags,
    projectIndex,
    toneIndex,
    formatIndex,
  });
};

export const parseUserHash = (userSettings: User['settings'], hash: string): User['settings'] => {
  const { languageIndex, verbose, useTags, projectIndex, toneIndex, formatIndex } = pickBy(
    parseHash(['languageIndex', 'verbose', 'useTags', 'projectIndex', 'toneIndex', 'formatIndex'], hash),
    (value) => value !== -1,
  );

  return {
    ...userSettings,
    language: Object.keys(Languages)[languageIndex as number],
    verbose: verbose as boolean,
    useTags: useTags as boolean,
    currentProjectName: userSettings.projects[projectIndex as number]?.name ?? null,
    tone: Object.keys(TONES)[toneIndex as number] ?? null,
    format: Object.keys(FORMATS)[formatIndex as number] ?? null,
  };
};
