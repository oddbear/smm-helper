import {
  DELETE_KEY_WORD,
  MAX_SYMBOLS_IN_PROJECT_DESCRIPTION,
  MAX_SYMBOLS_IN_PROJECT_NAME,
  MAX_WORDS_IN_PROJECT_NAME,
  HASH_SETTINGS_BREAKS,
  PRICE_FOR_CREDITS,
} from './constants';
import { User } from '../types/Schemas.type';
import { Languages } from './settings';
import { buildUserHash } from './hash';
import { escapeCharactersForTelegram } from './telegram';

export const dictionary = {
  startBot: '/start',
  backBtn: '💻 Назад',
  backBtnMessage: 'Вы находитесь в "Главном меню 💻".\nВыберите нужный пункт.',
  welcome: {
    text: 'Я - SMM-Helper, и я умею генерировать контент для Ваших проектов! Нажмите "Генерация 🔄", выберите тон и формат, после этого введите текст запроса и получите результат!',
  },
  internalError: {
    message: (message?: string, prevText?: string) =>
      `${
        prevText == null ? '' : `~${escapeCharactersForTelegram(prevText.replace(/\n/g, ' '))}~\n\n`
      }${escapeCharactersForTelegram(
        `❌ Произошла ошибка, попробуйте еще раз.\n\nТекст ошибки:\n`,
      )}||${escapeCharactersForTelegram(message == null ? '.' : `: ${message}`)}||`,
    hide: '👁️‍🗨️ Скрыть',
  },
  notFound: {
    message: (text: string) => `Ваш запрос: "\`${text}\`" не распознан, возможно Вам подойдут следующие действия`,
    options: ['Генерация 🔄', 'F.A.Q. ❓'],
  },
  generate: {
    commandName: 'Генерация 🔄',
    backMessage: '⬆️ Текст успешно сгенерирован ⬆️',
    message: '⬇️ Введите текст запроса для генерации контента ⬇️',
    waitingForResponse: '⏳ Дождитесь ответа по предыдущему запросу',
    noCredits: {
      text: 'Недостаточно кредитов для генерации.',
      reply_markup: {
        inline_keyboard: [
          [
            {
              text: 'Пополнить баланс 💰',
              callback_data: 'Кредиты 💰',
            },
          ],
        ],
      },
    },
    generationWaiting: escapeCharactersForTelegram('Генерация...\n\nМеню появится после завершения процесса.'),
    settingsInfo: (settings: User['settings']) =>
      `${escapeCharactersForTelegram(
        `Настройки:${
          settings.currentProjectName == null ? '' : `\n- проект: ${settings.currentProjectName}`
        }\n- язык ответа: ${Languages[settings.language]}${
          settings.tone == null ? '' : `\n- тон: ${dictionary.generate.tone[settings.tone](false)}`
        }${settings.format == null ? '' : `\n- формат: ${dictionary.generate.format[settings.format](false)}`}${
          settings.verbose ? '\n- длинный текст' : ''
        }${
          settings.format === 'facebook post' || settings.format === 'blog post'
            ? `\n- ${settings.useTags ? 'с хэштегами' : 'без хэштегов'}`
            : ''
        }`,
      )}\n\n||${escapeCharactersForTelegram(
        `${HASH_SETTINGS_BREAKS[0]}${buildUserHash(settings)}${HASH_SETTINGS_BREAKS[1]}`,
      )}||`,
    settingsInfoWithoutMarkup: (settings: User['settings']) =>
      `Настройки:${
        settings.currentProjectName == null ? '' : `\n- проект: ${settings.currentProjectName}`
      }\n- язык ответа: ${Languages[settings.language]}${
        settings.tone == null ? '' : `\n- тон: ${dictionary.generate.tone[settings.tone](false)}`
      }${settings.format == null ? '' : `\n- формат: ${dictionary.generate.format[settings.format](false)}`}${
        settings.verbose ? '\n- длинный текст' : ''
      }${
        settings.format === 'facebook post' || settings.format === 'blog post'
          ? `\n- ${settings.useTags ? 'с хэштегами' : 'без хэштегов'}`
          : ''
      }\n\n${HASH_SETTINGS_BREAKS[0]}${buildUserHash(settings)}${HASH_SETTINGS_BREAKS[1]}`,
    getImages: {
      message: `Добавить изображение (${PRICE_FOR_CREDITS.image} кред.) 🖼️`,
      generationWaiting: 'Генерация изображения...\n\nМеню появится после завершения процесса.',
      repeat: 'Повторить 🔁',
    },
    repeat: 'Повторить 🔁',
    fulfill: 'Дополнить 🖊️',
    fulfillInfo:
      'Чтобы дополнить/изменить/удалить сгенерированный контент - ответьте на соответствующее сообщение и введите желаемые поправки.',
    complain: {
      commandName: 'Пожаловаться 📣',
      success:
        '📣 Спасибо за ваше обращение, я обязательно его рассмотрю и верну вам кредиты, если сообщение имело недостаточно информации или нарушало человеческие права.',
    },
    next: {
      commandName: 'Продолжить ⏩',
    },
    tone: {
      message: 'Выберите тон текста.',
      emoji: '🎵',
      humorous: (enabled: boolean) => `${enabled ? '✅' : ''} Юмор`,
      informative: (enabled: boolean) => `${enabled ? '✅' : ''} Информация`,
      formal: (enabled: boolean) => `${enabled ? '✅' : ''} Строгий`,
      positive: (enabled: boolean) => `${enabled ? '✅' : ''} Радость`,
      sad: (enabled: boolean) => `${enabled ? '✅' : ''} Грусть`,
      negative: (enabled: boolean) => `${enabled ? '✅' : ''} Злой`,
    },
    format: {
      message: 'Выберите формат текста.',
      emoji: '📝',
      'facebook post': (enabled: boolean) => `${enabled ? '✅' : ''} Соцсети`,
      'blog post': (enabled: boolean) => `${enabled ? '✅' : ''} Описание`,
      'long text': (enabled: boolean) => `${enabled ? '✅' : ''} Текст`,
      email: (enabled: boolean) => `${enabled ? '✅' : ''} Письмо`,
      faq: (enabled: boolean) => `${enabled ? '✅' : ''} FAQ`,
      idea: (enabled: boolean) => `${enabled ? '✅' : ''} Идея`,
      improve: (enabled: boolean) => `${enabled ? '✅' : ''} Улучшить`,
      expand: (enabled: boolean) => `${enabled ? '✅' : ''} Дополнить`,
      shorten: (enabled: boolean) => `${enabled ? '✅' : ''} Сократить`,
      rephrase: (enabled: boolean) => `${enabled ? '✅' : ''} Переделать`,
      'question and answer': (enabled: boolean) => `${enabled ? '✅' : ''} Вопрос-ответ`,
      list: (enabled: boolean) => `${enabled ? '✅' : ''} Список`,
      comparison: (enabled: boolean) => `${enabled ? '✅' : ''} Сравнение`,
      'how-to guide': (enabled: boolean) => `${enabled ? '✅' : ''} Инструкция`,
      'top 10/5/3 list': (enabled: boolean) => `${enabled ? '✅' : ''} Топ`,
      testimonial: (enabled: boolean) => `${enabled ? '✅' : ''} Отзыв`,
      news: (enabled: boolean) => `${enabled ? '✅' : ''} Новость`,
      'story/narrative': (enabled: boolean) => `${enabled ? '✅' : ''} История`,
    },
  },
  credits: {
    commandName: 'Кредиты 💰',
    description: 'Кредиты позволяют Вам генерировать контент.',
    balance: (amount: number) =>
      `Ваш баланс: ${amount} кредитов.\n\nВы можете приобрести дополнительные кредиты для того, чтобы продолжить пользоваться сервисом.`,
    paymentError: '❌ Произошла ошибка, пожалуйста, свяжитесь с администратором - @random_you.',
    paymentSuccessful: '✅💰 Оплата прошла успешно, ваш баланс обновлен.',
  },
  settings: {
    commandName: 'Настройки ⚙️',
    backBtn: '⚙️ Назад',
    projects: {
      commandName: 'Проекты 📽️',
      backBtn: '📽 Назад',
      project: {
        commandName: (name: string, isDefault: boolean) => `${isDefault ? '✅ ' : ''}"${name}"`,
        structureName: 'Проект 📷️',
        projectInfo: (name: string, isDefault: boolean, description: string | null) =>
          `Проект "${name}"${isDefault ? ' (по умолчанию)' : ''}.${
            description != null ? `\nОписание: "${description}".` : ''
          }`,
        edit: {
          commandName: (index: number) => `Редактировать описание #${index + 1}  📷️`,
          message: `Введите описание проекта длиной не более ${MAX_SYMBOLS_IN_PROJECT_DESCRIPTION} символов:`,
          invalid: `Длина описания не должна превышать ${MAX_SYMBOLS_IN_PROJECT_DESCRIPTION} символов!`,
          success: (name: string) => `Описание проекта "${name}" изменено.`,
        },
        delete: {
          commandName: (index: number) => `Удалить #${index + 1}  📷️`,
          message: `Введите "${DELETE_KEY_WORD}", если уверены, что хотите удалить проект:`,
          invalid: 'Неправильное контрольное слово.',
          success: (name: string) => `Проект "${name}" удален.`,
        },
        set: {
          commandName: (index: number) => `Использовать #${index + 1}  📷️`,
          success: `Проект установлен по умолчанию.`,
        },
        unset: {
          commandName: (index: number) => `Не использовать #${index + 1}  📷️`,
          success: 'Проект больше не используется по умолчанию.',
        },
      },
      add: {
        commandName: 'Добавить проект 📽️',
        message: `Введите название нового проекта, оно не должно превышать ${MAX_SYMBOLS_IN_PROJECT_NAME} символов и ${MAX_WORDS_IN_PROJECT_NAME} слов.`,
        invalid: `Название проекта не должно превышать ${MAX_SYMBOLS_IN_PROJECT_NAME} символов и ${MAX_WORDS_IN_PROJECT_NAME} слов.`,
        alreadyExists: (name: string) => `Проект "${name}" уже существует.`,
        success: 'Проект добавлен, теперь вы можете установить описание для него.',
      },
    },
    post: {
      commandName: 'Настройка постов 🔧',
      message: 'Выберите настройки для генерации постов.',
      language: (lang: string) => `Язык ответа: ${lang}`,
      verbose: (enabled: boolean) => `Длинный ответ (+${PRICE_FOR_CREDITS.verbose} кред.) ${enabled ? '✅' : '❌'}`,
      useTags: (enabled: boolean) => `Хэштеги в блогах ${enabled ? '✅' : '❌'}`,
    },
  },
  help: {
    commandName: 'F.A.Q. ❓',
    message: [
      {
        title: 'Как пользоваться ботом?',
        text: 'Прежде всего вы можете сгенерировать',
      },
    ].reduce(
      (acc, obj) =>
        `${acc === '' ? '' : acc}\n\n*${escapeCharactersForTelegram(obj.title)}*\n${escapeCharactersForTelegram(
          obj.text,
        )}`,
      '',
    ),
  },
  fulfillCompletion: {
    commandName: 'Дополнить 🖊',
    buildMessage: (aiText: string, userText: string) =>
      `Don't use markup like "User:"/"AI:"/"Answer on text:"/etc. just give a response.\n\nAI: ${aiText}\n\nUser: ${userText}`,
  },
  placeholders: {
    choose: (catalogName: string) =>
      `Вы находитесь в разделе "${catalogName}".\nВыберите подходящий раздел или интересующую услугу.`,
    searchVariants: (command: string) => `По запросу "${command}" были подобраны наиболее похожие варианты:`,
    nothingFound: (command: string) =>
      `По запросу "${command}" ничего не найдено.\nИспользуйте навигационное меню или воспользуйтесь помощью:`,
    sendCommandService: 'Выберите подходящий пункт меню',
    more: (emoji: string) => ({
      commandName: `${emoji} Еще`,
      message: 'Полный список',
    }),
  },
  defaultCommands: {
    help: 'Помощь по боту',
    restart: 'Перезапустить бота',
  },
};
