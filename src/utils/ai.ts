import { config } from '../config';
import { ChatGPTAPI as ChatGPTAPIType } from 'chatgpt';
import { dictionary } from './dictionary';
import { escapeCharactersForTelegram, handleError } from './telegram';
import {
  ADD_IMAGES_COMMAND,
  COMPLAIN_COMMAND,
  PRICE_FOR_CREDITS,
  REPEAT_COMPLETION_COMMAND,
  FULFILL_COMPLETION_COMMAND,
} from './constants';
import { User } from '../types/Schemas.type';
import TelegramBot from 'node-telegram-bot-api';
import { updateUser } from '../models/user.model';
import { addAnalyticsEvent } from './analytics';
import { ClusterContext, EventNames } from '../types/ClusterContext.type';

// eslint-disable-next-line @typescript-eslint/no-implied-eval
const _importDynamic = new Function('modulePath', 'return import(modulePath)');
let ChatGPTAPI: undefined | (new (any) => ChatGPTAPIType);

let initModulesWaiter: Promise<void> | undefined = undefined;
const initModules = async () => {
  if (initModulesWaiter == null) {
    initModulesWaiter = new Promise(async (resolve) => {
      // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
      if (globalThis.fetch == null) {
        const { default: fetch } = await _importDynamic('node-fetch');
        globalThis.fetch = fetch;
      }

      if (ChatGPTAPI == null) {
        const chatgpt = await (_importDynamic('chatgpt') as Promise<{ ChatGPTAPI: new (any) => ChatGPTAPIType }>);
        ChatGPTAPI = chatgpt.ChatGPTAPI;
      }

      resolve();
    });
  }

  return initModulesWaiter;
};

let actualAIChat: ChatGPTAPIType | undefined = undefined;
export const getAIChat = async () => {
  await initModules();

  return (
    actualAIChat ??
    (actualAIChat = new ChatGPTAPI!({
      apiKey: config.openai.token,
    }))
  );
};

export const sendCompletionWithSideEffects = async (
  input: {
    telegramBot: TelegramBot;
    context: ClusterContext;
    user: User;
    replyId: number;
    text: string;
    disableRepeatButton?: boolean;
  },
  callback?: (err?: Error) => void,
): Promise<boolean> => {
  const { telegramBot, context, user, replyId, text, disableRepeatButton } = input;
  const { chatId } = user;

  try {
    if (user.credits < PRICE_FOR_CREDITS.regular) {
      void addAnalyticsEvent('notEnoughCredits');
      const removeMessage = await telegramBot.sendMessage(
        chatId,
        dictionary.generate.noCredits.text,
        dictionary.generate.noCredits,
      );

      const removeListener = context.addEventListener(EventNames.loopEnd, async () => {
        removeListener();

        await updateUser(user.chatId, {
          $push: {
            removeMessagesIds: removeMessage.message_id,
          },
        });
      });

      throw new Error('Not enough credits');
    }

    if (user.structureMessageId != null) {
      try {
        await telegramBot.deleteMessage(chatId, user.structureMessageId.toString());
      } catch (err) {}
    }
    await updateUser(chatId, {
      structureMessageId: null,
    });

    const responseMessage = await telegramBot.sendMessage(
      chatId,
      `${dictionary.generate.generationWaiting}\n\n${dictionary.generate.settingsInfo(user.settings)}`,
      {
        parse_mode: 'MarkdownV2',
        reply_to_message_id: replyId,
      },
    );

    try {
      const { verbose, useTags, currentProjectName, projects, tone, format, language } = user.settings;
      let prompt = '';

      let price: number = PRICE_FOR_CREDITS.regular;
      if (verbose) {
        price += PRICE_FOR_CREDITS.verbose;
      }

      if (currentProjectName != null) {
        const project = projects.find((p) => p.name === currentProjectName);
        if (project == null) {
          throw new Error(`Project with name ${currentProjectName} not found`);
        }

        prompt += `Organization Name: ${project.name}\n\n`;
        if (project.description != null) {
          prompt += `Organization Description: ${project.description}\n\n`;
        }
      }

      prompt += 'Customization Options:\n';
      if (format === 'facebook post' || format === 'blog post') {
        prompt += useTags ? '- With Hashtags\n' : '- Without Hashtags\n';
      }
      if (tone != null) {
        prompt += `- Tone: ${tone}\n`;
      }
      if (format != null) {
        prompt += `- Format: ${format}\n`;
      }
      prompt += verbose ? '- Your answer must be from 500 to 3000 Letters\n' : '- Use 1-500 Letters\n';

      prompt += `- Language: ${language}\n\nInput Text: {"\n${text}\n"}\n\nGenerate: ${format ?? 'post'}`;

      console.log('PROMPT', prompt);

      const analyticsValues: string[] = [];
      if (tone != null) {
        analyticsValues.push(`tone-${tone}`);
      }
      if (format != null) {
        analyticsValues.push(`format-${format}`);
      }

      void addAnalyticsEvent('preGenerate', analyticsValues);

      const aiChat = await getAIChat();
      await aiChat.sendMessage(prompt).then(async (data) => {
        const result = data.text;

        await updateUser(chatId, {
          $inc: {
            credits: -price,
          },
        });

        await telegramBot.editMessageText(
          `\`${escapeCharactersForTelegram(result)}\`\n\n${dictionary.generate.settingsInfo(user.settings)}`,
          {
            chat_id: chatId,
            message_id: responseMessage.message_id,
            parse_mode: 'MarkdownV2',
            reply_markup: {
              inline_keyboard: [
                [
                  {
                    text: dictionary.generate.getImages.message,
                    callback_data: ADD_IMAGES_COMMAND,
                  },
                ],
                [
                  ...(disableRepeatButton === true
                    ? []
                    : [
                        {
                          text: dictionary.generate.repeat,
                          callback_data: REPEAT_COMPLETION_COMMAND,
                        },
                      ]),
                  {
                    text: dictionary.generate.fulfill,
                    callback_data: FULFILL_COMPLETION_COMMAND,
                  },
                ],
                [
                  {
                    text: dictionary.generate.complain.commandName,
                    callback_data: COMPLAIN_COMMAND,
                  },
                ],
              ],
            },
          },
        );

        void addAnalyticsEvent('generate', analyticsValues);

        console.log(`PROMPT result: ${result}`);

        callback?.();
      });
    } catch (err) {
      await handleError(
        telegramBot,
        {
          chatId,
          replyId: responseMessage.message_id,
        },
        err as Error,
      );
      callback?.(err as Error);
    }
  } catch (err) {
    await handleError(
      telegramBot,
      {
        chatId,
      },
      err as Error,
    );
    callback?.(err as Error);
  }

  return true;
};

export const getAIPromptToImage = async (text: string): Promise<string> => {
  const promptText = `I want you to act as a prompt generator for Midjourney's artificial intelligence program. Your job is to provide detailed and creative descriptions that will inspire unique and interesting images from the AI. Keep in mind that the AI is capable of understanding a wide range of language and can interpret abstract concepts, so feel free to be as imaginative and descriptive as possible. For example, you could describe a scene from a futuristic city, or a surreal landscape filled with strange creatures. The more detailed and imaginative your description, the more interesting the resulting image will be. Here is your first prompt: "${text}". Answer on English please`;

  const aiChat = await getAIChat();
  const resultText = (await aiChat.sendMessage(promptText)).text;

  return `mdjrny-v4 style ${resultText}`;
};
