import { config } from '../config';
import mongoose from 'mongoose';
import { MongoError } from 'mongodb';

export const initializeMongo = (host = 'mongo'): void => {
  const mongoUri = `mongodb://${host}:27017/admin`;
  mongoose
    .connect(mongoUri, {
      user: config.mongo.username,
      pass: config.mongo.password,
    })
    .then(async () => {
      console.log('MongoDB connected successfully');
    })
    .catch((err: MongoError) => {
      console.error(`Error connection mongodb, mongoUri: ${mongoUri}, ${err.message}`);
    });
};
