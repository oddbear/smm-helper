import { AnalyticsEvent } from './constants';
import { getAnalytics } from '../models/analytics.model';

export const addAnalyticsEvent = async (eventName: AnalyticsEvent, valueNames?: string | string[]) => {
  const analytics = await getAnalytics();

  const event = analytics.events.find((e) => e.eventName === eventName);
  if (event == null) {
    throw new Error(`Event ${eventName} not found`);
  }

  event.count++;
  if (valueNames != null) {
    if (event.values == null) {
      throw new Error(`Event ${eventName} has no values`);
    }

    if (!(valueNames instanceof Array)) {
      valueNames = [valueNames];
    }
    for (const valueName of valueNames) {
      const value = event.values.find((value) => value.valueName === valueName);
      if (value == null) {
        throw new Error(`Value ${valueName} not found for eventName ${eventName}`);
      }
      value.count++;
    }
  }

  await analytics.save();
};

// (async function () {
//   console.log(JSON.stringify(await getAnalytics(), null, 2));
// })();
