import { User } from '../types/Schemas.type';
import { updateUser } from '../models/user.model';

export const Languages = {
  russian: 'Русский 🇷🇺',
  english: 'Английский 🇬🇧',
  ukranian: 'Украинский 🇺🇦',
  turkish: 'Турецкий 🇹🇷',
};

export const getNextLanguage = (user: User) => {
  const { language } = user.settings;
  const languages = Object.keys(Languages);
  const currentLanguageIndex = languages.indexOf(language);
  const nextLanguageIndex = currentLanguageIndex === languages.length - 1 ? 0 : currentLanguageIndex + 1;
  const nextLanguage = languages[nextLanguageIndex];

  return nextLanguage;
};
