import TelegramBot from 'node-telegram-bot-api';
import { User } from '../types/Schemas.type';
import { updateUser } from '../models/user.model';
import { dictionary } from './dictionary';
import { PRICE_FOR_CREDITS, REPEAT_IMAGE_COMMAND } from './constants';

const MODEL_NAME = 'prompthero/openjourney';
const MODEL_VERSION = '9936c2001faa2194a261c01381f90e65261879985476014a0a37a334593a05eb';
const TIMEOUT = 60;

// eslint-disable-next-line @typescript-eslint/no-implied-eval
const _importDynamic = new Function('modulePath', 'return import(modulePath)');

interface CookieJarInstance {
  cookies: Map<string, Map<string, { name?: string; value?: string }>>;
}
let CookieJar: new () => CookieJarInstance;

let fetch: (
  cookieJar: CookieJarInstance,
  url: string,
  options?: { [key: string]: unknown },
) => Promise<{ json: () => undefined | Promise<{ uuid: string; prediction?: { output?: string[] } }> }>;

let initModulesWaiter: Promise<void> | undefined = undefined;
const initModules = async () => {
  if (initModulesWaiter == null) {
    initModulesWaiter = new Promise(async (resolve) => {
      const module = await _importDynamic('node-fetch-cookies');

      // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
      fetch = module.fetch;
      // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
      CookieJar = module.CookieJar;

      resolve();
    });
  }

  return initModulesWaiter;
};

function sleep(milliseconds) {
  return new Promise((resolve) => setTimeout(resolve, milliseconds));
}

export default async function midjourneyRequest(
  prompt: string,
  inputs: { [key: string]: unknown } = {},
): Promise<string[]> {
  await initModules();

  const sessionCookieJar = new CookieJar();

  // Grab a CSRF token...

  await fetch(sessionCookieJar, `https://replicate.com/${MODEL_NAME}`);
  const cookie = sessionCookieJar.cookies.get('replicate.com')?.get('csrftoken')?.value ?? '';

  console.log(`Midjourney cookie used: ${cookie}`);

  // Call the model API...

  const response1 = await fetch(
    sessionCookieJar,
    `https://replicate.com/api/models/${MODEL_NAME}/versions/${MODEL_VERSION}/predictions`,
    {
      headers: {
        'content-type': 'application/json',
        accept: 'application/json',
        'x-csrftoken': cookie,
      },
      method: 'POST',
      mode: 'cors',
      credentials: 'include',
      body: JSON.stringify({
        inputs: {
          guidance_scale: '7',
          width: 512,
          height: 512,
          num_inference_steps: 150,
          num_outputs: 1,
          seed: null,
          prompt,
          ...inputs,
        },
      }),
    },
  );

  // Wait for the image to be generated...

  const result = await response1.json();
  const uuid = result?.uuid;
  if (uuid == null) {
    throw new Error(`Midjourney no UUID returned, response: ${JSON.stringify(result)}`);
  }

  for (let timeoutCounter = 0; timeoutCounter < TIMEOUT; timeoutCounter++) {
    const response2 = await fetch(
      sessionCookieJar,
      `https://replicate.com/api/models/${MODEL_NAME}/versions/${MODEL_VERSION}/predictions/${uuid}`,
      {
        headers: {
          accept: '*/*',
        },
        method: 'GET',
        mode: 'cors',
        credentials: 'include',
        body: null,
      },
    );

    const output = (await response2.json())?.prediction?.output;
    if (output != null && output.length > 0) {
      return output;
    }

    await sleep(1000);
  }

  throw new Error('Midjoruney generating timeout');
}

export const generateImage = async (
  telegramBot: TelegramBot,
  user: User,
  replyId: number,
  prompt: string,
  callback?: () => void,
) => {
  const result = await midjourneyRequest(prompt);

  await telegramBot.sendPhoto(user.chatId, result[0], {
    reply_to_message_id: replyId,
    reply_markup: {
      inline_keyboard: [
        [
          {
            text: dictionary.generate.getImages.repeat,
            callback_data: REPEAT_IMAGE_COMMAND,
          },
        ],
      ],
    },
  });

  await updateUser(user.chatId, {
    $inc: {
      credits: -PRICE_FOR_CREDITS.image,
    },
  });

  callback?.();
};
