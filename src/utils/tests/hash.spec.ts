import { buildHash, convertHashToValue, convertValueToHash, parseHash } from '../hash';

describe('hash', () => {
  it('should build hash', () => {
    const hash = buildHash({
      foo: 1,
      bar: 2,
      cat: 3,
    });

    expect(hash).toBe('efd');
  });

  it('should parse hash', () => {
    const hash = parseHash(['foo', 'bar', 'cat'], 'dfa');

    expect(hash).toEqual({
      bar: 1,
      cat: 3,
      foo: true,
    });
  });

  it('should convert value to hash', () => {
    const boolHash = convertValueToHash(false);
    const strHash = convertValueToHash(1);

    expect(boolHash).toBe('b');
    expect(strHash).toBe('d');
  });

  it('should convert hash to value', () => {
    const boolHash = convertHashToValue('a');
    const strHash = convertHashToValue('e');

    expect(boolHash).toBe(true);
    expect(strHash).toBe(2);
  });
});
