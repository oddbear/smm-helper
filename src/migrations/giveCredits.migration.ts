import { updateUsers } from '../models/user.model';

export const up = async () => {
  await updateUsers(
    {},
    {
      credits: 1,
    },
  );
};
