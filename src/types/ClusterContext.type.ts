import TelegramBot from 'node-telegram-bot-api';
import { CommandsStructure } from './CommadsStructure.type';

export enum EventNames {
  'loopEnd',
}

export interface ClusterContext {
  contextId: number;
  chatId: number;

  commandsStructure: CommandsStructure | null;
  commandStructure: CommandsStructure[number] | null;

  dispatchCallbackQuery: (context: ClusterContext, query: TelegramBot.CallbackQuery) => Promise<void>;
  addEventListener: (eventName: EventNames, callback: (context: ClusterContext) => unknown) => () => void;

  stopPropagation: boolean;
  dontRemoveMessage?: boolean;
}
