import TelegramBot from 'node-telegram-bot-api';
import { ServiceTool } from './Service.tool';

type Properties = { [key: string]: unknown };

export abstract class InputHandlerServiceTool extends ServiceTool {
  protected properties: Properties | undefined = undefined;

  public setOptions(
    options: Parameters<ServiceTool['setOptions']>[0] & {
      properties?: Properties | null;
    },
  ): this {
    super.setOptions(options);

    this.properties = options.properties ?? {};

    return this;
  }

  protected getOptions(): ReturnType<ServiceTool['getOptions']> & {
    properties?: Properties;
  } {
    const options = super.getOptions();

    return {
      ...options,
      properties: this.properties,
    };
  }

  public abstract activate(msg: TelegramBot.Message): Promise<boolean>;
}
