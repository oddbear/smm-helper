import { InputHandlerCommand } from '../utils/constants';
import { InputHandlerServiceTool } from './tools/InputHandlerService.tool';

export type InputHandlerOptions = {
  [key in InputHandlerCommand]?: InputHandlerServiceTool;
};
