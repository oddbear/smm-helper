import TelegramBot from 'node-telegram-bot-api';
import { ClusterContext } from './ClusterContext.type';

export type MessageType = Partial<TelegramBot.Message> & { text: string; parse_mode?: 'Markdown' | 'MarkdownV2' };
export type MessageFunctionType = (context: ClusterContext) => Promise<string | MessageType>;
export type UnionMessageType = string | MessageType | MessageFunctionType;
