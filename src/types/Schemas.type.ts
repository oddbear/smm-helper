import { Document } from 'mongoose';
import { AnalyticsEvent } from '../utils/constants';

export interface Project {
  name: string;
  description: string | null;
}

export interface User extends Document {
  chatId: number;
  keyboardType: 'default' | 'inline';
  structureMessageId: number | null;
  removeMessagesIds: number[];
  paymentMessageId: number;
  inputHandler: {
    command: string;
    options: { [key: string]: unknown } | null;
  } | null;
  savedMessages: {
    commandName: string;
    messageId: number;
  }[];
  isWaitingForResponse: boolean;
  credits: number;
  settings: {
    language: string;
    verbose: boolean;
    useTags: boolean;
    currentProjectName: string | null;
    projects: Project[];
    tone: string | null;
    format: string | null;
  };
}

export interface Analytics extends Document {
  events: {
    eventName: AnalyticsEvent;
    count: number;
    values?: {
      valueName: string;
      count: number;
    }[];
  }[];
  createdMonth: number;
}
